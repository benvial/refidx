


# refidx



<!-- start-badges -->

[![](https://img.shields.io/endpoint?url=https://gitlab.com/benvial/refidx/-/jobs/artifacts/main/raw/logobadge.json?job=badge&labelColor=c0c0c0)](https://gitlab.com/benvial/refidx/-/releases)
[![](https://img.shields.io/gitlab/pipeline-status/benvial/refidx?branch=main&logo=gitlab&labelColor=dedede&style=for-the-badge)](https://gitlab.com/benvial/refidx/commits/main)
[![](https://img.shields.io/gitlab/coverage/benvial/refidx/main?logo=python&logoColor=e9d672&style=for-the-badge)](https://benvial.gitlab.io/refidx/coverage.html)

[![](https://img.shields.io/badge/code%20style-black-dedede.svg?logo=python&logoColor=e9d672&style=for-the-badge)](https://black.readthedocs.io/en/stable/)
[![](https://img.shields.io/pypi/v/refidx?logo=python&logoColor=e9d672&style=for-the-badge)](https://pypi.org/project/refidx/)
[![](https://img.shields.io/pypi/dm/refidx?logo=python&logoColor=e9d672&style=for-the-badge)](https://pypi.org/project/refidx/)

[![](https://img.shields.io/badge/license-GPLv3-blue?color=5faa9c&logo=open-access&logoColor=white&style=for-the-badge)](https://gitlab.com/benvial/refidx/-/blob/main/LICENSE.txt)

<!-- end-badges -->


<!-- start-desc -->

Retrieve the refractive index of a material at a given wavelength
from the [refractiveindex.info](https://refractiveindex.info/) database.

<!-- end-desc -->


See documentation at [benvial.gitlab.io/refidx](https://benvial.gitlab.io/refidx).



<!-- start-cite -->

##  Citing

###  refidx


> B. Vial. refidx: Python package to retrieve the refractive index of material from the refractiveindex.info database. gitlab (2021). https://benvial.gitlab.io/refidx


Bibtex entry

```bibtex
@software{refidx,
  author   = {Vial, Benjamin},
  title    = {\texttt{refidx}: Python package to retrieve the refractive index of material from  the \texttt{refractiveindex.info} database},
  year     = {2021},
  journal  = {gitlab},
  url      = {https://benvial.gitlab.io/refidx},
}
```


###  refractiveindex.info database


> M. N. Polyanskiy. Refractiveindex.info database of optical constants. Sci. Data 11, 94 (2024). https://doi.org/10.1038/s41597-023-02898-2

Bibtex entry

```bibtex
@article{polyanskiy2024,
	title = {Refractiveindex.info database of optical constants},
	volume = {11},
	issn = {2052-4463},
	url = {https://www.nature.com/articles/s41597-023-02898-2},
	doi = {10.1038/s41597-023-02898-2},
	number = {1},
	journal = {Scientific Data},
	author = {Polyanskiy, Mikhail N.},
	month = jan,
	year = {2024},
	keywords = {Materials for optics, Optical materials and structures},
	pages = {94},
}

```


<!-- end-cite -->
