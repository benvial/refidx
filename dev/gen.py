#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of refidx
# License: GPLv3
# See the documentation at benvial.gitlab.io/refidx

import functools
import os

import numpy as np
import yaml
from parse import *
from yaml.reader import Reader

path = os.path.dirname(os.path.abspath(__file__))
database_nk_path = os.path.join(path, "database", "data-nk")
database_path = os.path.join(path, "database", "data")

os.system(f"mv {database_nk_path} {database_path}")


def fix_yml_file(yamlFile):
    if not yamlFile.endswith(".yml"):
        yamlFile += ".yml"
    return yamlFile


#
# def strip_invalid(s):
#     res = ""
#     for x in s:
#         if Reader.NON_PRINTABLE.match(x):
#             # res += '\\x{:x}'.format(ord(x))
#             continue
#         res += x
#     return res
#


def yaml_extract(yamlFile):
    yamlFile = fix_yml_file(yamlFile)
    filename = os.path.join(database_path, yamlFile)
    with open(filename) as yamlStream:
        c = yamlStream.read()
        # allData = yaml.safe_load(strip_invalid(c))
        allData = yaml.safe_load(c)
    materialData = allData["DATA"][0]
    return allData


def get_tabulated_data(materialData, datatype):
    assert materialData["type"] == "tabulated {}".format(datatype)

    matLambda = []
    matN = []
    matK = []
    # in this type of material read data line by line
    for line in materialData["data"].split("\n"):
        line = line.rstrip()
        line = line.replace(" .", " 0.")

        try:
            if datatype == "n":
                parsed = parse("{l:g} {n:g}", line)
                n = parsed["n"]
                matN.append(n)
                matK.append(0)
            elif datatype == "k":
                parsed = parse("{l:g} {k:g}", line)
                k = parsed["k"]
                matN.append(0)
                matK.append(k)
            else:
                parsed = parse("{l:g} {n:g} {k:g}", line)
                n = parsed["n"]
                k = parsed["k"]
                matN.append(n)
                matK.append(k)
            matLambda.append(parsed["l"])
        except TypeError:
            pass

    return np.array(matLambda), np.array(matN) + 1j * np.array(matK)


def get_formula_data(materialData, formula_number):
    assert materialData["type"] == "formula {}".format(formula_number)

    try:
        dataRange = np.array(list(map(float, materialData["range"].split())))
    except:
        dataRange = -np.inf, np.inf
    coeff = np.array(list(map(float, materialData["coefficients"].split())))
    return coeff  # formula(lamb, coeff, formula_number)


def get_directory_structure(rootdir):
    dir = {}
    dir1 = {}
    dir2 = {}
    materials_path = []
    materials_list = []
    rootdir = rootdir.rstrip(os.sep)
    start = rootdir.rfind(os.sep) + 1
    for path, dirs, files in os.walk(rootdir):
        folders = path[start:].split(os.sep)
        flist = []
        for f in files:
            if f.endswith(".yml") and f != "library.yml":
                flist.append(f[:-4])
                frel = os.path.join(os.path.relpath(path, database_path), f)
                materials_path.append(frel[:-4])
                materials_list.append(frel[:-4].split("/"))

        subdir = dict.fromkeys(flist)
        subdir1 = dict.fromkeys(flist)
        subdir2 = dict.fromkeys(flist)

        if np.all(list(subdir.values())) == None:
            subdir = list(subdir.keys())
        else:
            parent = functools.reduce(dict.get, folders[:-1], dir)
        parent[folders[-1]] = subdir
        parent1 = functools.reduce(dict.get, folders[:-1], dir1)
        parent1[folders[-1]] = subdir1
    return dir["data"], dir1["data"], materials_path


database_list, database, materials_path = get_directory_structure(database_path)


def format_mat(path):
    # path = os.path.join(*id)
    data = yaml_extract(path)
    materialData = data["DATA"][0]
    mattype = materialData["type"]
    if mattype.split()[0] == "tabulated":
        wls, index = get_tabulated_data(materialData, mattype.split()[1])
        ### list??
        # materialData["data"] = {}
        materialData.pop("data")
        materialData["wavelengths"] = wls.tolist()
        materialData["index"] = index.tolist()
        materialData["wavelength_range"] = [wls.min(), wls.max()]
    else:
        coeffs = get_formula_data(materialData, int(mattype.split()[1]))
        try:
            materialData["wavelength_range"] = [
                float(a) for a in materialData["wavelength_range"].split()
            ]
        except:
            materialData["wavelength_range"] = [
                float(a) for a in materialData["range"].split()
            ]
        materialData["coefficients"] = coeffs.tolist()

    data["DATA"] = materialData
    return data


def recursive_items(dictionary):
    for key, value in dictionary.items():
        if type(value) is dict:
            # yield (key, value, dictionary)
            yield from recursive_items(value)
        else:
            yield (key, value, dictionary)


j = 0
for key, value, dictionary in recursive_items(database):
    print(materials_path[j])
    if value == None:
        dictionary[key] = format_mat(materials_path[j])
    j += 1

# Open file in write mode
try:
    os.remove("rm database.npz")
except:
    pass
try:
    os.remove("rm ../src/refidx/database.npz")
except:
    pass
import numpy as np

np.savez(
    "database.npz",
    materials_path=materials_path,
    database=database,
    database_list=database_list,
)

os.system("mv database.npz ../src/refidx/")
