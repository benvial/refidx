#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: GPLv3

import os

import refidx as ri

db = ri.DataBase()
allmats = db.keys_list
for i, item in enumerate(allmats):
    if item[0] == "main":
        item_fixed = [
            i.replace("(", "_").replace(")", "_").replace(" ", "_").replace("-", "_")
            for i in item
        ]
        name = "_".join(item_fixed)
        filename = f"plot_{name}.py"
        mat = db.get_item(item)
        namedir = "/".join(item_fixed[1:-1])
        os.system(f"mkdir -p {namedir}")
        for iloc, loc in enumerate(item_fixed[1:-1]):
            locname = item_fixed[1 : iloc + 2]
            namesubdir = "/".join(locname)
            if iloc == 0:
                readme = rf"""
{locname[-1].capitalize()}
============================================================
"""
            with open(f"{namesubdir}/README.rst", "w") as fh:
                fh.write(readme)

        basepy = rf'''
#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of refidx
# License: GPLv3
# See the documentation at benvial.gitlab.io/refidx

"""
{item[-1]}
========================================================

Data for {mat.name}
"""

import os
import matplotlib.pyplot as plt
import numpy as np
import refidx as ri

plt.style.use("../../doc/refidx.mplstyle")
db = ri.DataBase()
matid = {item}
mat = db.get_item(matid)
wr = mat.wavelength_range
lamb = np.linspace(*wr, 1000)
index = mat.get_index(lamb)
fig, ax = plt.subplots(2, 1, figsize=(3, 3))
ax[0].plot(lamb, index.real, "-", color="#aa0044")
ax[1].plot(lamb, index.imag, "-", color="#6886b3")
ax[0].set_xlabel(r"Wavelength ($\rm μm$)")
ax[1].set_xlabel(r"Wavelength ($\rm μm$)")
ax[0].set_ylabel(r"$n^{{\prime}}$")
ax[1].set_ylabel(r"$n^{{\prime\prime}}$")
plt.suptitle(mat)
mat.print_info(
    html=True,
    tmp_dir=os.path.join("..","..", "doc", "auto_gallery","{item_fixed[-2]}"),
    filename="out_{name}.html",
)

####################################
# .. raw:: html
#     :file: out_{name}.html
'''

        with open(f"{namedir}/{filename}", "w") as fh:
            fh.write(basepy)
