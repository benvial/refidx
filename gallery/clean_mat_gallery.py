#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: GPLv3

import os
import shutil

namedir = "."

for item in os.listdir(namedir):
    if item not in ["README.rst", "gen_mat_gallery.py", "clean_mat_gallery.py"]:
        itempath = os.path.join(namedir, item)
        if os.path.isfile(itempath):
            os.unlink(itempath)
        else:
            shutil.rmtree(itempath)
