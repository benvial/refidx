#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of refidx
# License: GPLv3
# See the documentation at benvial.gitlab.io/refidx

"""
Refractive index
================

Plot material properties.
"""

import os

import matplotlib.pyplot as plt
import numpy as np

import refidx as ri

###############################################################################
# Get the database

db = ri.DataBase()

###############################################################################
# The ``materials`` attribute is a nested dictionary with all the materials

mat = db.materials
mat.print()


print(mat.findkeys("Si"))

print(mat.find("Au"))


###############################################################################
# Get a material (an instance of the ``Material`` class). For example:

mat = db.materials["main"]["Au"]["Johnson"]
print(mat)


###############################################################################
# Print material information

print(mat.info)

###############################################################################
# Get the wavelength range

wr = mat.wavelength_range
print("wavelength range (in microns): ", wr)

###############################################################################
# Define wavelengths array

lamb = np.linspace(*wr, 1000)

###############################################################################
# Get the refractive index

index = mat.get_index(lamb)


###############################################################################
# Plot refractive index dispersion

plt.style.use("../doc/refidx.mplstyle")

fig, ax = plt.subplots(2, 1, figsize=(3, 3))
ax[0].plot(lamb, index.real, "-", color="#aa0044")
ax[1].plot(lamb, index.imag, "-", color="#6886b3")
ax[0].set_xlabel(r"Wavelength ($\rm μm$)")
ax[1].set_xlabel(r"Wavelength ($\rm μm$)")
ax[0].set_ylabel(r"$n^{\prime}$")
ax[1].set_ylabel(r"$n^{\prime\prime}$")
plt.suptitle(mat)
plt.tight_layout()
plt.show()


###############################################################################
# Add data reference and comments

mat.print_info(html=True, tmp_dir=os.path.join("..", "doc", "auto_examples"))


####################################
# .. raw:: html
#     :file: out.html
