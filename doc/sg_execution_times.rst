
:orphan:

.. _sphx_glr_sg_execution_times:


Computation times
=================
**09:57.682** total execution time for 1006 files **from all galleries**:

.. container::

  .. raw:: html

    <style scoped>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    </style>
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap5.min.js"></script>
    <script type="text/javascript" class="init">
    $(document).ready( function () {
        $('table.sg-datatable').DataTable({order: [[1, 'desc']]});
    } );
    </script>

  .. list-table::
   :header-rows: 1
   :class: table table-striped sg-datatable

   * - Example
     - Time
     - Mem (MB)
   * - :ref:`sphx_glr_auto_examples_plot_index.py` (``../examples/plot_index.py``)
     - 00:01.881
     - 243.9
   * - :ref:`sphx_glr_auto_gallery_ZrSe2_plot_main_ZrSe2_Zotev_o.py` (``../gallery/ZrSe2/plot_main_ZrSe2_Zotev_o.py``)
     - 00:01.220
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZrO2_plot_main_ZrO2_Bodurov.py` (``../gallery/ZrO2/plot_main_ZrO2_Bodurov.py``)
     - 00:01.196
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_solid_161.35K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_solid_161.35K.py``)
     - 00:01.077
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Grace_liquid_161.35K.py` (``../gallery/Xe/plot_main_Xe_Grace_liquid_161.35K.py``)
     - 00:01.058
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Zr_plot_main_Zr_Querry.py` (``../gallery/Zr/plot_main_Zr_Querry.py``)
     - 00:01.031
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_liquid_161.35K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_liquid_161.35K.py``)
     - 00:00.982
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnSe_plot_main_ZnSe_Connolly.py` (``../gallery/ZnSe/plot_main_ZnSe_Connolly.py``)
     - 00:00.777
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cu_plot_main_Cu_Hagemann.py` (``../gallery/Cu/plot_main_Cu_Hagemann.py``)
     - 00:00.742
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Tm_plot_main_Tm_Vidal_Dasilva.py` (``../gallery/Tm/plot_main_Tm_Vidal_Dasilva.py``)
     - 00:00.738
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ReS2_plot_main_ReS2_Munkhbat_gamma.py` (``../gallery/ReS2/plot_main_ReS2_Munkhbat_gamma.py``)
     - 00:00.737
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZrSe2_plot_main_ZrSe2_Zotev_e.py` (``../gallery/ZrSe2/plot_main_ZrSe2_Zotev_e.py``)
     - 00:00.736
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Eu_plot_main_Eu_Fernandez_Perea.py` (``../gallery/Eu/plot_main_Eu_Fernandez_Perea.py``)
     - 00:00.726
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_V_plot_main_V_Johnson.py` (``../gallery/V/plot_main_V_Johnson.py``)
     - 00:00.726
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Ermolaev_e.py` (``../gallery/MoS2/plot_main_MoS2_Ermolaev_e.py``)
     - 00:00.724
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Li_150K.py` (``../gallery/Ge/plot_main_Ge_Li_150K.py``)
     - 00:00.723
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnS_plot_main_ZnS_Querry.py` (``../gallery/ZnS/plot_main_ZnS_Querry.py``)
     - 00:00.722
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaP_plot_main_GaP_Parsons.py` (``../gallery/GaP/plot_main_GaP_Parsons.py``)
     - 00:00.722
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ne_plot_main_Ne_Cuthbertson.py` (``../gallery/Ne/plot_main_Ne_Cuthbertson.py``)
     - 00:00.720
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnS_plot_main_ZnS_Amotchkina.py` (``../gallery/ZnS/plot_main_ZnS_Amotchkina.py``)
     - 00:00.720
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Vuye_400C.py` (``../gallery/Si/plot_main_Si_Vuye_400C.py``)
     - 00:00.720
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YbF3_plot_main_YbF3_Amotchkina.py` (``../gallery/YbF3/plot_main_YbF3_Amotchkina.py``)
     - 00:00.719
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnSe_plot_main_ZnSe_Querry.py` (``../gallery/ZnSe/plot_main_ZnSe_Querry.py``)
     - 00:00.718
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiO2_plot_main_TiO2_Kischkat.py` (``../gallery/TiO2/plot_main_TiO2_Kischkat.py``)
     - 00:00.716
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Gu_4L.py` (``../gallery/WSe2/plot_main_WSe2_Gu_4L.py``)
     - 00:00.714
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WS2_plot_main_WS2_Vyshnevyy_e.py` (``../gallery/WS2/plot_main_WS2_Vyshnevyy_e.py``)
     - 00:00.712
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Y2O3_plot_main_Y2O3_Nigara.py` (``../gallery/Y2O3/plot_main_Y2O3_Nigara.py``)
     - 00:00.708
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_solid_100K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_solid_100K.py``)
     - 00:00.708
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si3N4_plot_main_Si3N4_Vogt_1.py` (``../gallery/Si3N4/plot_main_Si3N4_Vogt_1.py``)
     - 00:00.707
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YVO4_plot_main_YVO4_Birnbaum_e.py` (``../gallery/YVO4/plot_main_YVO4_Birnbaum_e.py``)
     - 00:00.706
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnWO4_plot_main_ZnWO4_Bond_beta.py` (``../gallery/ZnWO4/plot_main_ZnWO4_Bond_beta.py``)
     - 00:00.705
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnTe_plot_main_ZnTe_Marple.py` (``../gallery/ZnTe/plot_main_ZnTe_Marple.py``)
     - 00:00.705
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_750K.py` (``../gallery/Si/plot_main_Si_Li_750K.py``)
     - 00:00.704
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Zhang.py` (``../gallery/MoS2/plot_main_MoS2_Zhang.py``)
     - 00:00.703
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Koch.py` (``../gallery/Xe/plot_main_Xe_Koch.py``)
     - 00:00.703
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiNbO3_plot_main_LiNbO3_Zelmon_o.py` (``../gallery/LiNbO3/plot_main_LiNbO3_Zelmon_o.py``)
     - 00:00.702
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnO_plot_main_ZnO_Bond_e.py` (``../gallery/ZnO/plot_main_ZnO_Bond_e.py``)
     - 00:00.702
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_solid_130K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_solid_130K.py``)
     - 00:00.701
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnO_plot_main_ZnO_Stelling.py` (``../gallery/ZnO/plot_main_ZnO_Stelling.py``)
     - 00:00.699
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_VO2_plot_main_VO2_Oguntoye_40C.py` (``../gallery/VO2/plot_main_VO2_Oguntoye_40C.py``)
     - 00:00.698
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_VO2_plot_main_VO2_Oguntoye_20C.py` (``../gallery/VO2/plot_main_VO2_Oguntoye_20C.py``)
     - 00:00.697
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_VO2_plot_main_VO2_Oguntoye_50C.py` (``../gallery/VO2/plot_main_VO2_Oguntoye_50C.py``)
     - 00:00.697
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Gu_3L.py` (``../gallery/WSe2/plot_main_WSe2_Gu_3L.py``)
     - 00:00.697
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnO_plot_main_ZnO_Bond_o.py` (``../gallery/ZnO/plot_main_ZnO_Bond_o.py``)
     - 00:00.694
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Tl3AsSe3_plot_main_Tl3AsSe3_Ewbank_e.py` (``../gallery/Tl3AsSe3/plot_main_Tl3AsSe3_Ewbank_e.py``)
     - 00:00.694
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ScAlMgO4_plot_main_ScAlMgO4_Stefaniuk_e.py` (``../gallery/ScAlMgO4/plot_main_ScAlMgO4_Stefaniuk_e.py``)
     - 00:00.694
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WO3_plot_main_WO3_Kulikova.py` (``../gallery/WO3/plot_main_WO3_Kulikova.py``)
     - 00:00.693
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Gu_1L.py` (``../gallery/WSe2/plot_main_WSe2_Gu_1L.py``)
     - 00:00.691
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaAs_plot_main_GaAs_Rakic.py` (``../gallery/GaAs/plot_main_GaAs_Rakic.py``)
     - 00:00.691
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiO2_plot_main_TiO2_Jolivet_amorphous.py` (``../gallery/TiO2/plot_main_TiO2_Jolivet_amorphous.py``)
     - 00:00.691
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_W_plot_main_W_Ordal.py` (``../gallery/W/plot_main_W_Ordal.py``)
     - 00:00.691
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_solid_110K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_solid_110K.py``)
     - 00:00.690
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Hsu_1L.py` (``../gallery/WSe2/plot_main_WSe2_Hsu_1L.py``)
     - 00:00.690
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnGeP2_plot_main_ZnGeP2_Zelmon_o.py` (``../gallery/ZnGeP2/plot_main_ZnGeP2_Zelmon_o.py``)
     - 00:00.690
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_W_plot_main_W_Rakic_BB.py` (``../gallery/W/plot_main_W_Rakic_BB.py``)
     - 00:00.690
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Zn_plot_main_Zn_Querry.py` (``../gallery/Zn/plot_main_Zn_Querry.py``)
     - 00:00.690
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Zn_plot_main_Zn_Motulevich.py` (``../gallery/Zn/plot_main_Zn_Motulevich.py``)
     - 00:00.689
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_VO2_plot_main_VO2_Oguntoye_30C.py` (``../gallery/VO2/plot_main_VO2_Oguntoye_30C.py``)
     - 00:00.689
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_VO2_plot_main_VO2_Oguntoye_60C.py` (``../gallery/VO2/plot_main_VO2_Oguntoye_60C.py``)
     - 00:00.689
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_solid_120K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_solid_120K.py``)
     - 00:00.689
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Zn_plot_main_Zn_Werner.py` (``../gallery/Zn/plot_main_Zn_Werner.py``)
     - 00:00.689
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnTe_plot_main_ZnTe_Li.py` (``../gallery/ZnTe/plot_main_ZnTe_Li.py``)
     - 00:00.686
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnGeP2_plot_main_ZnGeP2_Boyd_20_e.py` (``../gallery/ZnGeP2/plot_main_ZnGeP2_Boyd_20_e.py``)
     - 00:00.686
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Ermolaev.py` (``../gallery/WSe2/plot_main_WSe2_Ermolaev.py``)
     - 00:00.686
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiO2_plot_main_TiO2_Devore_e.py` (``../gallery/TiO2/plot_main_TiO2_Devore_e.py``)
     - 00:00.685
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_V_plot_main_V_Werner_DFT.py` (``../gallery/V/plot_main_V_Werner_DFT.py``)
     - 00:00.685
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnSe_plot_main_ZnSe_Amotchkina.py` (``../gallery/ZnSe/plot_main_ZnSe_Amotchkina.py``)
     - 00:00.685
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SnS2_plot_main_SnS2_Ermolaev.py` (``../gallery/SnS2/plot_main_SnS2_Ermolaev.py``)
     - 00:00.685
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_W_plot_main_W_Werner.py` (``../gallery/W/plot_main_W_Werner.py``)
     - 00:00.684
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Jung.py` (``../gallery/WSe2/plot_main_WSe2_Jung.py``)
     - 00:00.683
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WS2_plot_main_WS2_Zotev_e.py` (``../gallery/WS2/plot_main_WS2_Zotev_e.py``)
     - 00:00.683
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_W_plot_main_W_Windt.py` (``../gallery/W/plot_main_W_Windt.py``)
     - 00:00.683
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_VO2_plot_main_VO2_Oguntoye_80C.py` (``../gallery/VO2/plot_main_VO2_Oguntoye_80C.py``)
     - 00:00.682
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ti_plot_main_Ti_Werner.py` (``../gallery/Ti/plot_main_Ti_Werner.py``)
     - 00:00.682
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiO2_plot_main_TiO2_Bond_e.py` (``../gallery/TiO2/plot_main_TiO2_Bond_e.py``)
     - 00:00.681
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiO2_plot_main_TiO2_Zhukovsky.py` (``../gallery/TiO2/plot_main_TiO2_Zhukovsky.py``)
     - 00:00.680
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Sn_plot_main_Sn_Golovashkin_4.2K.py` (``../gallery/Sn/plot_main_Sn_Golovashkin_4.2K.py``)
     - 00:00.680
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnGeP2_plot_main_ZnGeP2_Das_o.py` (``../gallery/ZnGeP2/plot_main_ZnGeP2_Das_o.py``)
     - 00:00.680
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnO_plot_main_ZnO_Aguilar.py` (``../gallery/ZnO/plot_main_ZnO_Aguilar.py``)
     - 00:00.680
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_solid_140K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_solid_140K.py``)
     - 00:00.680
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_VO2_plot_main_VO2_Beaini_100C.py` (``../gallery/VO2/plot_main_VO2_Beaini_100C.py``)
     - 00:00.679
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WS2_plot_main_WS2_Jung.py` (``../gallery/WS2/plot_main_WS2_Jung.py``)
     - 00:00.679
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiC_plot_main_SiC_Wang_6H_o.py` (``../gallery/SiC/plot_main_SiC_Wang_6H_o.py``)
     - 00:00.679
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ti_plot_main_Ti_Ordal.py` (``../gallery/Ti/plot_main_Ti_Ordal.py``)
     - 00:00.679
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_VO2_plot_main_VO2_Beaini_25C.py` (``../gallery/VO2/plot_main_VO2_Beaini_25C.py``)
     - 00:00.677
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ni_plot_main_Ni_Werner.py` (``../gallery/Ni/plot_main_Ni_Werner.py``)
     - 00:00.677
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SrMoO4_plot_main_SrMoO4_Bond_e.py` (``../gallery/SrMoO4/plot_main_SrMoO4_Bond_e.py``)
     - 00:00.677
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ne_plot_main_Ne_Bideau_Mehu.py` (``../gallery/Ne/plot_main_Ne_Bideau_Mehu.py``)
     - 00:00.677
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_VC_plot_main_VC_Pfluger.py` (``../gallery/VC/plot_main_VC_Pfluger.py``)
     - 00:00.676
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_W_plot_main_W_Weaver.py` (``../gallery/W/plot_main_W_Weaver.py``)
     - 00:00.676
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnTe_plot_main_ZnTe_Sato.py` (``../gallery/ZnTe/plot_main_ZnTe_Sato.py``)
     - 00:00.676
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Daub.py` (``../gallery/Si/plot_main_Si_Daub.py``)
     - 00:00.676
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_V_plot_main_V_Palm.py` (``../gallery/V/plot_main_V_Palm.py``)
     - 00:00.675
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiO2_plot_main_TiO2_Siefke.py` (``../gallery/TiO2/plot_main_TiO2_Siefke.py``)
     - 00:00.674
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TaS2_plot_main_TaS2_Munkhbat_e.py` (``../gallery/TaS2/plot_main_TaS2_Munkhbat_e.py``)
     - 00:00.674
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnGeP2_plot_main_ZnGeP2_Das_e.py` (``../gallery/ZnGeP2/plot_main_ZnGeP2_Das_e.py``)
     - 00:00.674
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO2_plot_main_SiO2_Malitson.py` (``../gallery/SiO2/plot_main_SiO2_Malitson.py``)
     - 00:00.674
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Te_plot_main_Te_Ciesielski.py` (``../gallery/Te/plot_main_Te_Ciesielski.py``)
     - 00:00.673
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Sc2O3_plot_main_Sc2O3_Belosludtsev.py` (``../gallery/Sc2O3/plot_main_Sc2O3_Belosludtsev.py``)
     - 00:00.673
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Grace_solid_161.35K.py` (``../gallery/Xe/plot_main_Xe_Grace_solid_161.35K.py``)
     - 00:00.673
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Sr_plot_main_Sr_Rodriguez_de_Marcos.py` (``../gallery/Sr/plot_main_Sr_Rodriguez_de_Marcos.py``)
     - 00:00.673
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YVO4_plot_main_YVO4_Birnbaum_o.py` (``../gallery/YVO4/plot_main_YVO4_Birnbaum_o.py``)
     - 00:00.673
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ti_plot_main_Ti_Rakic_BB.py` (``../gallery/Ti/plot_main_Ti_Rakic_BB.py``)
     - 00:00.672
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ScAlMgO4_plot_main_ScAlMgO4_Stefaniuk_o.py` (``../gallery/ScAlMgO4/plot_main_ScAlMgO4_Stefaniuk_o.py``)
     - 00:00.672
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Borzsonyi.py` (``../gallery/Xe/plot_main_Xe_Borzsonyi.py``)
     - 00:00.672
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WS2_plot_main_WS2_Ermolaev.py` (``../gallery/WS2/plot_main_WS2_Ermolaev.py``)
     - 00:00.672
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WS2_plot_main_WS2_Hsu_3L.py` (``../gallery/WS2/plot_main_WS2_Hsu_3L.py``)
     - 00:00.671
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Tl3AsSe3_plot_main_Tl3AsSe3_Ewbank_o.py` (``../gallery/Tl3AsSe3/plot_main_Tl3AsSe3_Ewbank_o.py``)
     - 00:00.671
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pd_plot_main_Pd_Rakic_BB.py` (``../gallery/Pd/plot_main_Pd_Rakic_BB.py``)
     - 00:00.671
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnS_plot_main_ZnS_Debenham.py` (``../gallery/ZnS/plot_main_ZnS_Debenham.py``)
     - 00:00.670
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_V_plot_main_V_Werner.py` (``../gallery/V/plot_main_V_Werner.py``)
     - 00:00.670
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Rb_plot_main_Rb_Yamaguchi.py` (``../gallery/Rb/plot_main_Rb_Yamaguchi.py``)
     - 00:00.670
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO2_plot_main_SiO2_Gao.py` (``../gallery/SiO2/plot_main_SiO2_Gao.py``)
     - 00:00.670
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Rh_plot_main_Rh_Coulter_300.py` (``../gallery/Rh/plot_main_Rh_Coulter_300.py``)
     - 00:00.670
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si3N4_plot_main_Si3N4_Vogt_3.py` (``../gallery/Si3N4/plot_main_Si3N4_Vogt_3.py``)
     - 00:00.669
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pd_plot_main_Pd_Johnson.py` (``../gallery/Pd/plot_main_Pd_Johnson.py``)
     - 00:00.668
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Sik_25C.py` (``../gallery/Si/plot_main_Si_Sik_25C.py``)
     - 00:00.668
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ta_plot_main_Ta_Windt.py` (``../gallery/Ta/plot_main_Ta_Windt.py``)
     - 00:00.668
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YVO4_plot_main_YVO4_Shi_e_110C.py` (``../gallery/YVO4/plot_main_YVO4_Shi_e_110C.py``)
     - 00:00.668
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WS2_plot_main_WS2_Hsu_1L.py` (``../gallery/WS2/plot_main_WS2_Hsu_1L.py``)
     - 00:00.667
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ti_plot_main_Ti_Werner_DFT.py` (``../gallery/Ti/plot_main_Ti_Werner_DFT.py``)
     - 00:00.667
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Se_plot_main_Se_Ciesielski.py` (``../gallery/Se/plot_main_Se_Ciesielski.py``)
     - 00:00.667
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WTe2_plot_main_WTe2_Munkhbat_gamma.py` (``../gallery/WTe2/plot_main_WTe2_Munkhbat_gamma.py``)
     - 00:00.667
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Os_plot_main_Os_Nemoshkalenko_e.py` (``../gallery/Os/plot_main_Os_Nemoshkalenko_e.py``)
     - 00:00.666
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Sc_plot_main_Sc_Larruquert.py` (``../gallery/Sc/plot_main_Sc_Larruquert.py``)
     - 00:00.666
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ta_plot_main_Ta_Ordal.py` (``../gallery/Ta/plot_main_Ta_Ordal.py``)
     - 00:00.666
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WS2_plot_main_WS2_Hsu_2L.py` (``../gallery/WS2/plot_main_WS2_Hsu_2L.py``)
     - 00:00.666
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Chandler_Horowitz.py` (``../gallery/Si/plot_main_Si_Chandler_Horowitz.py``)
     - 00:00.665
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Sn_plot_main_Sn_Golovashkin_78K.py` (``../gallery/Sn/plot_main_Sn_Golovashkin_78K.py``)
     - 00:00.665
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiO2_plot_main_TiO2_Jolivet_anatase.py` (``../gallery/TiO2/plot_main_TiO2_Jolivet_anatase.py``)
     - 00:00.665
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_solid_90K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_solid_90K.py``)
     - 00:00.665
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pd_plot_main_Pd_Rakic_LD.py` (``../gallery/Pd/plot_main_Pd_Rakic_LD.py``)
     - 00:00.665
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Gu_5L.py` (``../gallery/WSe2/plot_main_WSe2_Gu_5L.py``)
     - 00:00.665
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_450K.py` (``../gallery/Si/plot_main_Si_Li_450K.py``)
     - 00:00.665
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiN_plot_main_TiN_Beliaev.py` (``../gallery/TiN/plot_main_TiN_Beliaev.py``)
     - 00:00.665
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_W_plot_main_W_Werner_DFT.py` (``../gallery/W/plot_main_W_Werner_DFT.py``)
     - 00:00.664
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Munkhbat_e.py` (``../gallery/WSe2/plot_main_WSe2_Munkhbat_e.py``)
     - 00:00.664
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YVO4_plot_main_YVO4_Shi_e_50C.py` (``../gallery/YVO4/plot_main_YVO4_Shi_e_50C.py``)
     - 00:00.664
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiC_plot_main_SiC_Fischer_o.py` (``../gallery/SiC/plot_main_SiC_Fischer_o.py``)
     - 00:00.663
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_W_plot_main_W_Rakic_LD.py` (``../gallery/W/plot_main_W_Rakic_LD.py``)
     - 00:00.663
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WS2_plot_main_WS2_Munkhbat_e.py` (``../gallery/WS2/plot_main_WS2_Munkhbat_e.py``)
     - 00:00.663
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Rb_plot_main_Rb_Monin.py` (``../gallery/Rb/plot_main_Rb_Monin.py``)
     - 00:00.663
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si3N4_plot_main_Si3N4_Vogt_2.py` (``../gallery/Si3N4/plot_main_Si3N4_Vogt_2.py``)
     - 00:00.663
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnGeP2_plot_main_ZnGeP2_Boyd_70_e.py` (``../gallery/ZnGeP2/plot_main_ZnGeP2_Boyd_70_e.py``)
     - 00:00.663
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si3N4_plot_main_Si3N4_Philipp.py` (``../gallery/Si3N4/plot_main_Si3N4_Philipp.py``)
     - 00:00.663
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnSe_plot_main_ZnSe_Marple.py` (``../gallery/ZnSe/plot_main_ZnSe_Marple.py``)
     - 00:00.663
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnGeP2_plot_main_ZnGeP2_Boyd_20_o.py` (``../gallery/ZnGeP2/plot_main_ZnGeP2_Boyd_20_o.py``)
     - 00:00.662
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ti_plot_main_Ti_Palm.py` (``../gallery/Ti/plot_main_Ti_Palm.py``)
     - 00:00.662
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO_plot_main_SiO_Fernandez_Perea.py` (``../gallery/SiO/plot_main_SiO_Fernandez_Perea.py``)
     - 00:00.662
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Song_4L.py` (``../gallery/MoS2/plot_main_MoS2_Song_4L.py``)
     - 00:00.662
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Peck_15C.py` (``../gallery/Ar/plot_main_Ar_Peck_15C.py``)
     - 00:00.662
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiC_plot_main_SiC_Larruquert.py` (``../gallery/SiC/plot_main_SiC_Larruquert.py``)
     - 00:00.662
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TaS2_plot_main_TaS2_Munkhbat_o.py` (``../gallery/TaS2/plot_main_TaS2_Munkhbat_o.py``)
     - 00:00.661
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_650K.py` (``../gallery/Si/plot_main_Si_Li_650K.py``)
     - 00:00.661
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiC_plot_main_SiC_Singh_e.py` (``../gallery/SiC/plot_main_SiC_Singh_e.py``)
     - 00:00.661
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PbTe_plot_main_PbTe_Weiting_80K.py` (``../gallery/PbTe/plot_main_PbTe_Weiting_80K.py``)
     - 00:00.660
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SnSe2_plot_main_SnSe2_Ermolaev.py` (``../gallery/SnSe2/plot_main_SnSe2_Ermolaev.py``)
     - 00:00.660
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pb_plot_main_Pb_Werner.py` (``../gallery/Pb/plot_main_Pb_Werner.py``)
     - 00:00.660
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WTe2_plot_main_WTe2_Munkhbat_beta.py` (``../gallery/WTe2/plot_main_WTe2_Munkhbat_beta.py``)
     - 00:00.660
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ta2O5_plot_main_Ta2O5_Gao.py` (``../gallery/Ta2O5/plot_main_Ta2O5_Gao.py``)
     - 00:00.660
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Os_plot_main_Os_Windt.py` (``../gallery/Os/plot_main_Os_Windt.py``)
     - 00:00.659
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WTe2_plot_main_WTe2_Munkhbat_alpha.py` (``../gallery/WTe2/plot_main_WTe2_Munkhbat_alpha.py``)
     - 00:00.659
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiO2_plot_main_TiO2_Sarkar.py` (``../gallery/TiO2/plot_main_TiO2_Sarkar.py``)
     - 00:00.659
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SrTiO3_plot_main_SrTiO3_Dodge.py` (``../gallery/SrTiO3/plot_main_SrTiO3_Dodge.py``)
     - 00:00.659
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Vuye_250C.py` (``../gallery/Si/plot_main_Si_Vuye_250C.py``)
     - 00:00.658
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Green_1995.py` (``../gallery/Si/plot_main_Si_Green_1995.py``)
     - 00:00.658
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Gu_2L.py` (``../gallery/WSe2/plot_main_WSe2_Gu_2L.py``)
     - 00:00.658
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WS2_plot_main_WS2_Zotev_o.py` (``../gallery/WS2/plot_main_WS2_Zotev_o.py``)
     - 00:00.658
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ReS2_plot_main_ReS2_Munkhbat_alpha.py` (``../gallery/ReS2/plot_main_ReS2_Munkhbat_alpha.py``)
     - 00:00.658
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SnSe_plot_main_SnSe_Guo_beta.py` (``../gallery/SnSe/plot_main_SnSe_Guo_beta.py``)
     - 00:00.658
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnSiAs2_plot_main_ZnSiAs2_Boyd_o.py` (``../gallery/ZnSiAs2/plot_main_ZnSiAs2_Boyd_o.py``)
     - 00:00.658
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SnSe_plot_main_SnSe_Guo_alpha.py` (``../gallery/SnSe/plot_main_SnSe_Guo_alpha.py``)
     - 00:00.657
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ti_plot_main_Ti_Windt.py` (``../gallery/Ti/plot_main_Ti_Windt.py``)
     - 00:00.657
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ni_plot_main_Ni_Johnson.py` (``../gallery/Ni/plot_main_Ni_Johnson.py``)
     - 00:00.656
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_RbCl_plot_main_RbCl_Li.py` (``../gallery/RbCl/plot_main_RbCl_Li.py``)
     - 00:00.656
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pb_plot_main_Pb_Mathewson_298K.py` (``../gallery/Pb/plot_main_Pb_Mathewson_298K.py``)
     - 00:00.656
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TaSe2_plot_main_TaSe2_Munkhbat_e.py` (``../gallery/TaSe2/plot_main_TaSe2_Munkhbat_e.py``)
     - 00:00.656
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ta2O5_plot_main_Ta2O5_Bright_amorphous.py` (``../gallery/Ta2O5/plot_main_Ta2O5_Bright_amorphous.py``)
     - 00:00.656
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ta_plot_main_Ta_Werner.py` (``../gallery/Ta/plot_main_Ta_Werner.py``)
     - 00:00.656
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YVO4_plot_main_YVO4_Shi_e_140C.py` (``../gallery/YVO4/plot_main_YVO4_Shi_e_140C.py``)
     - 00:00.656
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si3N4_plot_main_Si3N4_Beliaev.py` (``../gallery/Si3N4/plot_main_Si3N4_Beliaev.py``)
     - 00:00.656
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO2_plot_main_SiO2_Arosa.py` (``../gallery/SiO2/plot_main_SiO2_Arosa.py``)
     - 00:00.655
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnO_plot_main_ZnO_Bodurov.py` (``../gallery/ZnO/plot_main_ZnO_Bodurov.py``)
     - 00:00.655
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ti3C2_plot_main_Ti3C2_Panova.py` (``../gallery/Ti3C2/plot_main_Ti3C2_Panova.py``)
     - 00:00.655
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_VO2_plot_main_VO2_Oguntoye_70C.py` (``../gallery/VO2/plot_main_VO2_Oguntoye_70C.py``)
     - 00:00.655
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Yb_plot_main_Yb_Larruquert.py` (``../gallery/Yb/plot_main_Yb_Larruquert.py``)
     - 00:00.655
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Nb_plot_main_Nb_Windt.py` (``../gallery/Nb/plot_main_Nb_Windt.py``)
     - 00:00.655
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiC_plot_main_SiC_Fischer_e.py` (``../gallery/SiC/plot_main_SiC_Fischer_e.py``)
     - 00:00.655
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiN_plot_main_TiN_Pfluger.py` (``../gallery/TiN/plot_main_TiN_Pfluger.py``)
     - 00:00.655
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TlBr_plot_main_TlBr_Palik.py` (``../gallery/TlBr/plot_main_TlBr_Palik.py``)
     - 00:00.654
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KH2PO4_plot_main_KH2PO4_Zernike_o.py` (``../gallery/KH2PO4/plot_main_KH2PO4_Zernike_o.py``)
     - 00:00.654
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WS2_plot_main_WS2_Vyshnevyy_o.py` (``../gallery/WS2/plot_main_WS2_Vyshnevyy_o.py``)
     - 00:00.654
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pt_plot_main_Pt_Rakic_LD.py` (``../gallery/Pt/plot_main_Pt_Rakic_LD.py``)
     - 00:00.654
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Islam_e.py` (``../gallery/MoS2/plot_main_MoS2_Islam_e.py``)
     - 00:00.654
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_RbTiOPO4_plot_main_RbTiOPO4_Carvajal_alpha.py` (``../gallery/RbTiOPO4/plot_main_RbTiOPO4_Carvajal_alpha.py``)
     - 00:00.654
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO2_plot_main_SiO2_Ghosh_o.py` (``../gallery/SiO2/plot_main_SiO2_Ghosh_o.py``)
     - 00:00.653
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Re_plot_main_Re_Windt.py` (``../gallery/Re/plot_main_Re_Windt.py``)
     - 00:00.653
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Zotev_e.py` (``../gallery/MoS2/plot_main_MoS2_Zotev_e.py``)
     - 00:00.653
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_VN_plot_main_VN_Pfluger.py` (``../gallery/VN/plot_main_VN_Pfluger.py``)
     - 00:00.653
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Sik_850C.py` (``../gallery/Si/plot_main_Si_Sik_850C.py``)
     - 00:00.652
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ReS2_plot_main_ReS2_Munkhbat_beta.py` (``../gallery/ReS2/plot_main_ReS2_Munkhbat_beta.py``)
     - 00:00.652
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoO3_plot_main_MoO3_Stelling.py` (``../gallery/MoO3/plot_main_MoO3_Stelling.py``)
     - 00:00.652
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoO3_plot_main_MoO3_Vos.py` (``../gallery/MoO3/plot_main_MoO3_Vos.py``)
     - 00:00.652
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Vuye_300C.py` (``../gallery/Si/plot_main_Si_Vuye_300C.py``)
     - 00:00.651
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KH2PO4_plot_main_KH2PO4_Zernike_e.py` (``../gallery/KH2PO4/plot_main_KH2PO4_Zernike_e.py``)
     - 00:00.651
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO2_plot_main_SiO2_Lemarchand.py` (``../gallery/SiO2/plot_main_SiO2_Lemarchand.py``)
     - 00:00.651
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Smith.py` (``../gallery/Kr/plot_main_Kr_Smith.py``)
     - 00:00.651
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ru_plot_main_Ru_Windt.py` (``../gallery/Ru/plot_main_Ru_Windt.py``)
     - 00:00.651
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO2_plot_main_SiO2_Rodriguez_de_Marcos.py` (``../gallery/SiO2/plot_main_SiO2_Rodriguez_de_Marcos.py``)
     - 00:00.651
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SrF2_plot_main_SrF2_Kaiser.py` (``../gallery/SrF2/plot_main_SrF2_Kaiser.py``)
     - 00:00.651
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Se_plot_main_Se_Campel_e.py` (``../gallery/Se/plot_main_Se_Campel_e.py``)
     - 00:00.650
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Sc2O3_plot_main_Sc2O3_Medenbach.py` (``../gallery/Sc2O3/plot_main_Sc2O3_Medenbach.py``)
     - 00:00.650
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Vuye_350C.py` (``../gallery/Si/plot_main_Si_Vuye_350C.py``)
     - 00:00.650
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_NH4H2PO4_plot_main_NH4H2PO4_Zernike_o.py` (``../gallery/NH4H2PO4/plot_main_NH4H2PO4_Zernike_o.py``)
     - 00:00.650
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Yb2O3_plot_main_Yb2O3_Medenbach.py` (``../gallery/Yb2O3/plot_main_Yb2O3_Medenbach.py``)
     - 00:00.650
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiC_plot_main_TiC_Pfluger.py` (``../gallery/TiC/plot_main_TiC_Pfluger.py``)
     - 00:00.650
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ThF4_plot_main_ThF4_Heitmann.py` (``../gallery/ThF4/plot_main_ThF4_Heitmann.py``)
     - 00:00.650
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Rb_plot_main_Rb_Smith.py` (``../gallery/Rb/plot_main_Rb_Smith.py``)
     - 00:00.649
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnWO4_plot_main_ZnWO4_Bond_gamma.py` (``../gallery/ZnWO4/plot_main_ZnWO4_Bond_gamma.py``)
     - 00:00.649
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO2_plot_main_SiO2_Popova.py` (``../gallery/SiO2/plot_main_SiO2_Popova.py``)
     - 00:00.649
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgF2_plot_main_MgF2_Zheng_e_186C.py` (``../gallery/MgF2/plot_main_MgF2_Zheng_e_186C.py``)
     - 00:00.649
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiO2_plot_main_TiO2_Bodurov.py` (``../gallery/TiO2/plot_main_TiO2_Bodurov.py``)
     - 00:00.648
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiN_plot_main_TiN_Shkondin_900.py` (``../gallery/TiN/plot_main_TiN_Shkondin_900.py``)
     - 00:00.648
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Yim_20nm.py` (``../gallery/MoS2/plot_main_MoS2_Yim_20nm.py``)
     - 00:00.648
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiN_plot_main_TiN_Shkondin_700.py` (``../gallery/TiN/plot_main_TiN_Shkondin_700.py``)
     - 00:00.648
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Rh_plot_main_Rh_Windt.py` (``../gallery/Rh/plot_main_Rh_Windt.py``)
     - 00:00.648
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Sn_plot_main_Sn_Golovashkin_293K.py` (``../gallery/Sn/plot_main_Sn_Golovashkin_293K.py``)
     - 00:00.648
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YVO4_plot_main_YVO4_Shi_o_50C.py` (``../gallery/YVO4/plot_main_YVO4_Shi_o_50C.py``)
     - 00:00.647
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO2_plot_main_SiO2_Ghosh_e.py` (``../gallery/SiO2/plot_main_SiO2_Ghosh_e.py``)
     - 00:00.647
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Pierce.py` (``../gallery/Si/plot_main_Si_Pierce.py``)
     - 00:00.647
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pb5Ge3O11_plot_main_Pb5Ge3O11_Simon_o.py` (``../gallery/Pb5Ge3O11/plot_main_Pb5Ge3O11_Simon_o.py``)
     - 00:00.647
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnGeP2_plot_main_ZnGeP2_Zelmon_e.py` (``../gallery/ZnGeP2/plot_main_ZnGeP2_Zelmon_e.py``)
     - 00:00.647
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Sc2O3_plot_main_Sc2O3_Arndt.py` (``../gallery/Sc2O3/plot_main_Sc2O3_Arndt.py``)
     - 00:00.647
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_O2_plot_main_O2_Smith.py` (``../gallery/O2/plot_main_O2_Smith.py``)
     - 00:00.647
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO2_plot_main_SiO2_Radhakrishnan_e.py` (``../gallery/SiO2/plot_main_SiO2_Radhakrishnan_e.py``)
     - 00:00.647
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnSiAs2_plot_main_ZnSiAs2_Boyd_e.py` (``../gallery/ZnSiAs2/plot_main_ZnSiAs2_Boyd_e.py``)
     - 00:00.647
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Rb_plot_main_Rb_Whang.py` (``../gallery/Rb/plot_main_Rb_Whang.py``)
     - 00:00.647
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ta2O5_plot_main_Ta2O5_Bright_nanocrystalline.py` (``../gallery/Ta2O5/plot_main_Ta2O5_Bright_nanocrystalline.py``)
     - 00:00.646
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PbMoO4_plot_main_PbMoO4_Malitson_o.py` (``../gallery/PbMoO4/plot_main_PbMoO4_Malitson_o.py``)
     - 00:00.646
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TeO2_plot_main_TeO2_Uchida_e.py` (``../gallery/TeO2/plot_main_TeO2_Uchida_e.py``)
     - 00:00.645
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_600K.py` (``../gallery/Si/plot_main_Si_Li_600K.py``)
     - 00:00.645
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiC_plot_main_SiC_Wang_4H_e.py` (``../gallery/SiC/plot_main_SiC_Wang_4H_e.py``)
     - 00:00.645
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Nb2O5_plot_main_Nb2O5_Horcholle_400.py` (``../gallery/Nb2O5/plot_main_Nb2O5_Horcholle_400.py``)
     - 00:00.645
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnGeP2_plot_main_ZnGeP2_Boyd_70_o.py` (``../gallery/ZnGeP2/plot_main_ZnGeP2_Boyd_70_o.py``)
     - 00:00.645
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PbMoO4_plot_main_PbMoO4_Malitson_e.py` (``../gallery/PbMoO4/plot_main_PbMoO4_Malitson_e.py``)
     - 00:00.645
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YVO4_plot_main_YVO4_Shi_o_110C.py` (``../gallery/YVO4/plot_main_YVO4_Shi_o_110C.py``)
     - 00:00.645
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiO2_plot_main_TiO2_Bond_o.py` (``../gallery/TiO2/plot_main_TiO2_Bond_o.py``)
     - 00:00.645
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Hale.py` (``../gallery/H2O/plot_main_H2O_Hale.py``)
     - 00:00.644
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TlBr_plot_main_TlBr_Schroter.py` (``../gallery/TlBr/plot_main_TlBr_Schroter.py``)
     - 00:00.644
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Rh_plot_main_Rh_Coulter_40.py` (``../gallery/Rh/plot_main_Rh_Coulter_40.py``)
     - 00:00.644
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO_plot_main_SiO_Hass.py` (``../gallery/SiO/plot_main_SiO_Hass.py``)
     - 00:00.644
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnWO4_plot_main_ZnWO4_Bond_alpha.py` (``../gallery/ZnWO4/plot_main_ZnWO4_Bond_alpha.py``)
     - 00:00.644
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ir_plot_main_Ir_Roberts.py` (``../gallery/Ir/plot_main_Ir_Roberts.py``)
     - 00:00.644
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Zotev_o.py` (``../gallery/WSe2/plot_main_WSe2_Zotev_o.py``)
     - 00:00.644
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Aspnes.py` (``../gallery/Si/plot_main_Si_Aspnes.py``)
     - 00:00.644
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pt_plot_main_Pt_Werner.py` (``../gallery/Pt/plot_main_Pt_Werner.py``)
     - 00:00.644
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_N2_plot_main_N2_Borzsonyi.py` (``../gallery/N2/plot_main_N2_Borzsonyi.py``)
     - 00:00.643
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SrF2_plot_main_SrF2_Feldman.py` (``../gallery/SrF2/plot_main_SrF2_Feldman.py``)
     - 00:00.643
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PtSe2_plot_main_PtSe2_Ermolaev.py` (``../gallery/PtSe2/plot_main_PtSe2_Ermolaev.py``)
     - 00:00.643
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_solid_150K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_solid_150K.py``)
     - 00:00.642
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Na_plot_main_Na_Monin.py` (``../gallery/Na/plot_main_Na_Monin.py``)
     - 00:00.642
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ti_plot_main_Ti_Rakic_LD.py` (``../gallery/Ti/plot_main_Ti_Rakic_LD.py``)
     - 00:00.642
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KI_plot_main_KI_Li.py` (``../gallery/KI/plot_main_KI_Li.py``)
     - 00:00.642
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Zotev_o.py` (``../gallery/MoS2/plot_main_MoS2_Zotev_o.py``)
     - 00:00.642
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_293K.py` (``../gallery/Si/plot_main_Si_Li_293K.py``)
     - 00:00.642
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Zn_plot_main_Zn_Werner_DFT.py` (``../gallery/Zn/plot_main_Zn_Werner_DFT.py``)
     - 00:00.642
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoSe2_plot_main_MoSe2_Munkhbat_o.py` (``../gallery/MoSe2/plot_main_MoSe2_Munkhbat_o.py``)
     - 00:00.641
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Mo_plot_main_Mo_Werner_DFT.py` (``../gallery/Mo/plot_main_Mo_Werner_DFT.py``)
     - 00:00.641
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_400K.py` (``../gallery/Si/plot_main_Si_Li_400K.py``)
     - 00:00.641
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PbSe_plot_main_PbSe_Zemel.py` (``../gallery/PbSe/plot_main_PbSe_Zemel.py``)
     - 00:00.641
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoTe2_plot_main_MoTe2_Munkhbat_o.py` (``../gallery/MoTe2/plot_main_MoTe2_Munkhbat_o.py``)
     - 00:00.641
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Na_plot_main_Na_Inagaki.py` (``../gallery/Na/plot_main_Na_Inagaki.py``)
     - 00:00.641
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_NaF_plot_main_NaF_Li.py` (``../gallery/NaF/plot_main_NaF_Li.py``)
     - 00:00.641
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_RbTiOPO4_plot_main_RbTiOPO4_Carvajal_beta.py` (``../gallery/RbTiOPO4/plot_main_RbTiOPO4_Carvajal_beta.py``)
     - 00:00.641
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Nb2O5_plot_main_Nb2O5_Lemarchand.py` (``../gallery/Nb2O5/plot_main_Nb2O5_Lemarchand.py``)
     - 00:00.641
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ta_plot_main_Ta_Werner_DFT.py` (``../gallery/Ta/plot_main_Ta_Werner_DFT.py``)
     - 00:00.640
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Na_plot_main_Na_Althoff.py` (``../gallery/Na/plot_main_Na_Althoff.py``)
     - 00:00.640
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_NaI_plot_main_NaI_Li.py` (``../gallery/NaI/plot_main_NaI_Li.py``)
     - 00:00.640
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgF2_plot_main_MgF2_Zheng_o_368C.py` (``../gallery/MgF2/plot_main_MgF2_Zheng_o_368C.py``)
     - 00:00.639
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoSe2_plot_main_MoSe2_Beal.py` (``../gallery/MoSe2/plot_main_MoSe2_Beal.py``)
     - 00:00.639
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnO_plot_main_ZnO_Querry.py` (``../gallery/ZnO/plot_main_ZnO_Querry.py``)
     - 00:00.639
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pb_plot_main_Pb_Golovashkin_293K.py` (``../gallery/Pb/plot_main_Pb_Golovashkin_293K.py``)
     - 00:00.639
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pb_plot_main_Pb_Golovashkin_78K.py` (``../gallery/Pb/plot_main_Pb_Golovashkin_78K.py``)
     - 00:00.639
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KNbO3_plot_main_KNbO3_Zysset_alpha.py` (``../gallery/KNbO3/plot_main_KNbO3_Zysset_alpha.py``)
     - 00:00.639
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiH2_plot_main_TiH2_Palm.py` (``../gallery/TiH2/plot_main_TiH2_Palm.py``)
     - 00:00.639
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Hsu_3L.py` (``../gallery/MoS2/plot_main_MoS2_Hsu_3L.py``)
     - 00:00.639
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Hf_plot_main_Hf_Windt.py` (``../gallery/Hf/plot_main_Hf_Windt.py``)
     - 00:00.639
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Ciesielski_20nm.py` (``../gallery/Ge/plot_main_Ge_Ciesielski_20nm.py``)
     - 00:00.638
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiN_plot_main_TiN_Shkondin_800.py` (``../gallery/TiN/plot_main_TiN_Shkondin_800.py``)
     - 00:00.638
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pb_plot_main_Pb_Werner_DFT.py` (``../gallery/Pb/plot_main_Pb_Werner_DFT.py``)
     - 00:00.638
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Munkhbat_e.py` (``../gallery/MoS2/plot_main_MoS2_Munkhbat_e.py``)
     - 00:00.638
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_NaI_plot_main_NaI_Jellison.py` (``../gallery/NaI/plot_main_NaI_Jellison.py``)
     - 00:00.638
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WS2_plot_main_WS2_Munkhbat_o.py` (``../gallery/WS2/plot_main_WS2_Munkhbat_o.py``)
     - 00:00.638
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PbTe_plot_main_PbTe_Weiting_300K.py` (``../gallery/PbTe/plot_main_PbTe_Weiting_300K.py``)
     - 00:00.638
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pt_plot_main_Pt_Werner_DFT.py` (``../gallery/Pt/plot_main_Pt_Werner_DFT.py``)
     - 00:00.638
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_350K.py` (``../gallery/Si/plot_main_Si_Li_350K.py``)
     - 00:00.638
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Nb2O5_plot_main_Nb2O5_Horcholle_800.py` (``../gallery/Nb2O5/plot_main_Nb2O5_Horcholle_800.py``)
     - 00:00.637
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ti_plot_main_Ti_Mash.py` (``../gallery/Ti/plot_main_Ti_Mash.py``)
     - 00:00.637
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pd_plot_main_Pd_Windt.py` (``../gallery/Pd/plot_main_Pd_Windt.py``)
     - 00:00.637
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Sinnock_solid_75K.py` (``../gallery/Kr/plot_main_Kr_Sinnock_solid_75K.py``)
     - 00:00.637
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Cuthbertson.py` (``../gallery/Kr/plot_main_Kr_Cuthbertson.py``)
     - 00:00.637
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si3N4_plot_main_Si3N4_Kischkat.py` (``../gallery/Si3N4/plot_main_Si3N4_Kischkat.py``)
     - 00:00.637
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Te_plot_main_Te_Guo_e.py` (``../gallery/Te/plot_main_Te_Guo_e.py``)
     - 00:00.637
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Warren_1984.py` (``../gallery/H2O/plot_main_H2O_Warren_1984.py``)
     - 00:00.637
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Vuye_150C.py` (``../gallery/Si/plot_main_Si_Vuye_150C.py``)
     - 00:00.637
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Na_plot_main_Na_Inagaki_liquid.py` (``../gallery/Na/plot_main_Na_Inagaki_liquid.py``)
     - 00:00.636
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_K_plot_main_K_Monin.py` (``../gallery/K/plot_main_K_Monin.py``)
     - 00:00.636
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiN_plot_main_TiN_Shkondin.py` (``../gallery/TiN/plot_main_TiN_Shkondin.py``)
     - 00:00.636
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ir_plot_main_Ir_Windt.py` (``../gallery/Ir/plot_main_Ir_Windt.py``)
     - 00:00.636
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Asfar.py` (``../gallery/H2O/plot_main_H2O_Asfar.py``)
     - 00:00.636
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_NH3_plot_main_NH3_Cuthbertson.py` (``../gallery/NH3/plot_main_NH3_Cuthbertson.py``)
     - 00:00.635
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Vuye_100C.py` (``../gallery/Si/plot_main_Si_Vuye_100C.py``)
     - 00:00.635
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pd_plot_main_Pd_Werner_DFT.py` (``../gallery/Pd/plot_main_Pd_Werner_DFT.py``)
     - 00:00.635
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_solid_80K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_solid_80K.py``)
     - 00:00.635
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PbTiO3_plot_main_PbTiO3_Singh_o.py` (``../gallery/PbTiO3/plot_main_PbTiO3_Singh_o.py``)
     - 00:00.635
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_In_plot_main_In_Mathewson_140K.py` (``../gallery/In/plot_main_In_Mathewson_140K.py``)
     - 00:00.634
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Hsu_1L.py` (``../gallery/MoS2/plot_main_MoS2_Hsu_1L.py``)
     - 00:00.634
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoTe2_plot_main_MoTe2_Munkhbat_e.py` (``../gallery/MoTe2/plot_main_MoTe2_Munkhbat_e.py``)
     - 00:00.634
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Vuye_20C.py` (``../gallery/Si/plot_main_Si_Vuye_20C.py``)
     - 00:00.634
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Sinnock_solid_85K.py` (``../gallery/Kr/plot_main_Kr_Sinnock_solid_85K.py``)
     - 00:00.633
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_RbF_plot_main_RbF_Li.py` (``../gallery/RbF/plot_main_RbF_Li.py``)
     - 00:00.633
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Na_plot_main_Na_Ives.py` (``../gallery/Na/plot_main_Na_Ives.py``)
     - 00:00.633
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Sik_500C.py` (``../gallery/Si/plot_main_Si_Sik_500C.py``)
     - 00:00.633
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoSe2_plot_main_MoSe2_Zotev_o.py` (``../gallery/MoSe2/plot_main_MoSe2_Zotev_o.py``)
     - 00:00.633
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BiB3O6_plot_main_BiB3O6_Umemura_alpha.py` (``../gallery/BiB3O6/plot_main_BiB3O6_Umemura_alpha.py``)
     - 00:00.633
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Li_400K.py` (``../gallery/Ge/plot_main_Ge_Li_400K.py``)
     - 00:00.633
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Song_8L.py` (``../gallery/MoS2/plot_main_MoS2_Song_8L.py``)
     - 00:00.633
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SrTiO3_plot_main_SrTiO3_Bond.py` (``../gallery/SrTiO3/plot_main_SrTiO3_Bond.py``)
     - 00:00.632
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Os_plot_main_Os_Nemoshkalenko_o.py` (``../gallery/Os/plot_main_Os_Nemoshkalenko_o.py``)
     - 00:00.632
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Jellison.py` (``../gallery/Si/plot_main_Si_Jellison.py``)
     - 00:00.632
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_InAs_plot_main_InAs_Aspnes.py` (``../gallery/InAs/plot_main_InAs_Aspnes.py``)
     - 00:00.632
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ti_plot_main_Ti_Johnson.py` (``../gallery/Ti/plot_main_Ti_Johnson.py``)
     - 00:00.632
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoSe2_plot_main_MoSe2_Hsu_1L.py` (``../gallery/MoSe2/plot_main_MoSe2_Hsu_1L.py``)
     - 00:00.632
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Li_250K.py` (``../gallery/Ge/plot_main_Ge_Li_250K.py``)
     - 00:00.632
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaN_plot_main_GaN_Lin_wurtzite.py` (``../gallery/GaN/plot_main_GaN_Lin_wurtzite.py``)
     - 00:00.632
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Yim_2nm.py` (``../gallery/MoS2/plot_main_MoS2_Yim_2nm.py``)
     - 00:00.632
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_He_plot_main_He_Mansfield.py` (``../gallery/He/plot_main_He_Mansfield.py``)
     - 00:00.632
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_NH4H2PO4_plot_main_NH4H2PO4_Zernike_e.py` (``../gallery/NH4H2PO4/plot_main_NH4H2PO4_Zernike_e.py``)
     - 00:00.631
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_RbTiOPO4_plot_main_RbTiOPO4_Carvajal_gamma.py` (``../gallery/RbTiOPO4/plot_main_RbTiOPO4_Carvajal_gamma.py``)
     - 00:00.631
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge2Sb2Te5_plot_main_Ge2Sb2Te5_Frantz_amorphous.py` (``../gallery/Ge2Sb2Te5/plot_main_Ge2Sb2Te5_Frantz_amorphous.py``)
     - 00:00.631
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoSe2_plot_main_MoSe2_Munkhbat_e.py` (``../gallery/MoSe2/plot_main_MoSe2_Munkhbat_e.py``)
     - 00:00.631
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TlCl_plot_main_TlCl_Schroter.py` (``../gallery/TlCl/plot_main_TlCl_Schroter.py``)
     - 00:00.631
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ta2O5_plot_main_Ta2O5_Rodriguez_de_Marcos.py` (``../gallery/Ta2O5/plot_main_Ta2O5_Rodriguez_de_Marcos.py``)
     - 00:00.631
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiC_plot_main_SiC_Wang_4H_o.py` (``../gallery/SiC/plot_main_SiC_Wang_4H_o.py``)
     - 00:00.631
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_He_plot_main_He_Cuthbertson.py` (``../gallery/He/plot_main_He_Cuthbertson.py``)
     - 00:00.631
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pb_plot_main_Pb_Mathewson_140K.py` (``../gallery/Pb/plot_main_Pb_Mathewson_140K.py``)
     - 00:00.630
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Bideau_Mehu.py` (``../gallery/Xe/plot_main_Xe_Bideau_Mehu.py``)
     - 00:00.630
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO2_plot_main_SiO2_Radhakrishnan_o.py` (``../gallery/SiO2/plot_main_SiO2_Radhakrishnan_o.py``)
     - 00:00.630
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Rowe_240K.py` (``../gallery/H2O/plot_main_H2O_Rowe_240K.py``)
     - 00:00.630
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_D2O_plot_main_D2O_Kedenburg.py` (``../gallery/D2O/plot_main_D2O_Kedenburg.py``)
     - 00:00.630
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Nb_plot_main_Nb_Golovashkin_78K.py` (``../gallery/Nb/plot_main_Nb_Golovashkin_78K.py``)
     - 00:00.630
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Kofman_50K.py` (``../gallery/H2O/plot_main_H2O_Kofman_50K.py``)
     - 00:00.630
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Nb_plot_main_Nb_Golovashkin_4.2K.py` (``../gallery/Nb/plot_main_Nb_Golovashkin_4.2K.py``)
     - 00:00.630
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_K_plot_main_K_Sutherland.py` (``../gallery/K/plot_main_K_Sutherland.py``)
     - 00:00.629
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Segelstein.py` (``../gallery/H2O/plot_main_H2O_Segelstein.py``)
     - 00:00.629
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YVO4_plot_main_YVO4_Shi_o_140C.py` (``../gallery/YVO4/plot_main_YVO4_Shi_o_140C.py``)
     - 00:00.629
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TeO2_plot_main_TeO2_Uchida_o.py` (``../gallery/TeO2/plot_main_TeO2_Uchida_o.py``)
     - 00:00.629
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CS2_plot_main_CS2_Kedenburg.py` (``../gallery/CS2/plot_main_CS2_Kedenburg.py``)
     - 00:00.629
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_VO2_plot_main_VO2_Oguntoye_55C.py` (``../gallery/VO2/plot_main_VO2_Oguntoye_55C.py``)
     - 00:00.629
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Song_bulk.py` (``../gallery/MoS2/plot_main_MoS2_Song_bulk.py``)
     - 00:00.629
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Shkondin.py` (``../gallery/Si/plot_main_Si_Shkondin.py``)
     - 00:00.629
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Edwards.py` (``../gallery/Si/plot_main_Si_Edwards.py``)
     - 00:00.628
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Te_plot_main_Te_Sherman_e.py` (``../gallery/Te/plot_main_Te_Sherman_e.py``)
     - 00:00.628
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TaSe2_plot_main_TaSe2_Munkhbat_o.py` (``../gallery/TaSe2/plot_main_TaSe2_Munkhbat_o.py``)
     - 00:00.628
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Na_plot_main_Na_Smith.py` (``../gallery/Na/plot_main_Na_Smith.py``)
     - 00:00.628
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Se_plot_main_Se_Campel_o.py` (``../gallery/Se/plot_main_Se_Campel_o.py``)
     - 00:00.628
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiC_plot_main_SiC_Singh_o.py` (``../gallery/SiC/plot_main_SiC_Singh_o.py``)
     - 00:00.628
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Munkhbat_o.py` (``../gallery/WSe2/plot_main_WSe2_Munkhbat_o.py``)
     - 00:00.627
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pd_plot_main_Pd_Werner.py` (``../gallery/Pd/plot_main_Pd_Werner.py``)
     - 00:00.627
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PdSe2_plot_main_PdSe2_Ermolaev.py` (``../gallery/PdSe2/plot_main_PdSe2_Ermolaev.py``)
     - 00:00.627
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Grace_solid_80K.py` (``../gallery/Xe/plot_main_Xe_Grace_solid_80K.py``)
     - 00:00.626
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Rh_plot_main_Rh_Weaver.py` (``../gallery/Rh/plot_main_Rh_Weaver.py``)
     - 00:00.626
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiO2_plot_main_TiO2_Devore_o.py` (``../gallery/TiO2/plot_main_TiO2_Devore_o.py``)
     - 00:00.626
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Sinnock_solid_67K.py` (``../gallery/Kr/plot_main_Kr_Sinnock_solid_67K.py``)
     - 00:00.626
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Te_plot_main_Te_Caldwell_e.py` (``../gallery/Te/plot_main_Te_Caldwell_e.py``)
     - 00:00.626
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pt_plot_main_Pt_Windt.py` (``../gallery/Pt/plot_main_Pt_Windt.py``)
     - 00:00.626
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaSe_plot_main_GaSe_Kato_o.py` (``../gallery/GaSe/plot_main_GaSe_Kato_o.py``)
     - 00:00.625
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiC_plot_main_SiC_Wang_6H_e.py` (``../gallery/SiC/plot_main_SiC_Wang_6H_e.py``)
     - 00:00.625
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Amotchkina_film.py` (``../gallery/Ge/plot_main_Ge_Amotchkina_film.py``)
     - 00:00.625
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Song_1L.py` (``../gallery/MoS2/plot_main_MoS2_Song_1L.py``)
     - 00:00.625
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PbTe_plot_main_PbTe_Weiting_130K.py` (``../gallery/PbTe/plot_main_PbTe_Weiting_130K.py``)
     - 00:00.625
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Islam_fewL.py` (``../gallery/MoS2/plot_main_MoS2_Islam_fewL.py``)
     - 00:00.625
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SnSe_plot_main_SnSe_Guo_gamma.py` (``../gallery/SnSe/plot_main_SnSe_Guo_gamma.py``)
     - 00:00.625
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_N2_plot_main_N2_Peck_0C.py` (``../gallery/N2/plot_main_N2_Peck_0C.py``)
     - 00:00.624
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaSe_plot_main_GaSe_Kato_e.py` (``../gallery/GaSe/plot_main_GaSe_Kato_e.py``)
     - 00:00.624
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_NaBr_plot_main_NaBr_Li.py` (``../gallery/NaBr/plot_main_NaBr_Li.py``)
     - 00:00.624
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Li_500K.py` (``../gallery/Ge/plot_main_Ge_Li_500K.py``)
     - 00:00.624
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgF2_plot_main_MgF2_Rodriguez_de_Marcos.py` (``../gallery/MgF2/plot_main_MgF2_Rodriguez_de_Marcos.py``)
     - 00:00.624
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Rh_plot_main_Rh_Arndt.py` (``../gallery/Rh/plot_main_Rh_Arndt.py``)
     - 00:00.623
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoTe2_plot_main_MoTe2_Beal.py` (``../gallery/MoTe2/plot_main_MoTe2_Beal.py``)
     - 00:00.623
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si3N4_plot_main_Si3N4_Luke.py` (``../gallery/Si3N4/plot_main_Si3N4_Luke.py``)
     - 00:00.623
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaP_plot_main_GaP_Khmelevskaia.py` (``../gallery/GaP/plot_main_GaP_Khmelevskaia.py``)
     - 00:00.622
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_InSb_plot_main_InSb_Djurisic.py` (``../gallery/InSb/plot_main_InSb_Djurisic.py``)
     - 00:00.622
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Islam_1L.py` (``../gallery/MoS2/plot_main_MoS2_Islam_1L.py``)
     - 00:00.622
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PbI2_plot_main_PbI2_Frisenda.py` (``../gallery/PbI2/plot_main_PbI2_Frisenda.py``)
     - 00:00.622
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BeAl2O4_plot_main_BeAl2O4_Walling_beta.py` (``../gallery/BeAl2O4/plot_main_BeAl2O4_Walling_beta.py``)
     - 00:00.621
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SrF2_plot_main_SrF2_Rodriguez_de_Marcos.py` (``../gallery/SrF2/plot_main_SrF2_Rodriguez_de_Marcos.py``)
     - 00:00.621
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Mo_plot_main_Mo_Kirillova.py` (``../gallery/Mo/plot_main_Mo_Kirillova.py``)
     - 00:00.621
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Yim_3nm.py` (``../gallery/MoS2/plot_main_MoS2_Yim_3nm.py``)
     - 00:00.621
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnS_plot_main_ZnS_Bond.py` (``../gallery/ZnS/plot_main_ZnS_Bond.py``)
     - 00:00.621
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PtS2_plot_main_PtS2_Ermolaev.py` (``../gallery/PtS2/plot_main_PtS2_Ermolaev.py``)
     - 00:00.621
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Nb_plot_main_Nb_Golovashkin_293K.py` (``../gallery/Nb/plot_main_Nb_Golovashkin_293K.py``)
     - 00:00.621
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_RbI_plot_main_RbI_Li.py` (``../gallery/RbI/plot_main_RbI_Li.py``)
     - 00:00.621
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BiFeO3_plot_main_BiFeO3_Kumar.py` (``../gallery/BiFeO3/plot_main_BiFeO3_Kumar.py``)
     - 00:00.621
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Koch.py` (``../gallery/Kr/plot_main_Kr_Koch.py``)
     - 00:00.621
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KNbO3_plot_main_KNbO3_Umemura_gamma.py` (``../gallery/KNbO3/plot_main_KNbO3_Umemura_gamma.py``)
     - 00:00.620
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Borzsonyi.py` (``../gallery/Kr/plot_main_Kr_Borzsonyi.py``)
     - 00:00.620
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgF2_plot_main_MgF2_Li_o.py` (``../gallery/MgF2/plot_main_MgF2_Li_o.py``)
     - 00:00.620
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_liquid_178K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_liquid_178K.py``)
     - 00:00.620
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Ordal.py` (``../gallery/Au/plot_main_Au_Ordal.py``)
     - 00:00.620
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SF6_plot_main_SF6_Vukovic.py` (``../gallery/SF6/plot_main_SF6_Vukovic.py``)
     - 00:00.620
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoSe2_plot_main_MoSe2_Zotev_e.py` (``../gallery/MoSe2/plot_main_MoSe2_Zotev_e.py``)
     - 00:00.620
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnS_plot_main_ZnS_Ozaki.py` (``../gallery/ZnS/plot_main_ZnS_Ozaki.py``)
     - 00:00.620
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Rowe_253K.py` (``../gallery/H2O/plot_main_H2O_Rowe_253K.py``)
     - 00:00.619
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Cuthbertson.py` (``../gallery/Ar/plot_main_Ar_Cuthbertson.py``)
     - 00:00.619
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_K_plot_main_K_Ives.py` (``../gallery/K/plot_main_K_Ives.py``)
     - 00:00.619
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pd_plot_main_Pd_Palm.py` (``../gallery/Pd/plot_main_Pd_Palm.py``)
     - 00:00.618
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SrF2_plot_main_SrF2_Li.py` (``../gallery/SrF2/plot_main_SrF2_Li.py``)
     - 00:00.618
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaP_plot_main_GaP_Jellison.py` (``../gallery/GaP/plot_main_GaP_Jellison.py``)
     - 00:00.618
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Mo_plot_main_Mo_Querry.py` (``../gallery/Mo/plot_main_Mo_Querry.py``)
     - 00:00.618
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_150K.py` (``../gallery/Si/plot_main_Si_Li_150K.py``)
     - 00:00.618
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Sik_250C.py` (``../gallery/Si/plot_main_Si_Sik_250C.py``)
     - 00:00.618
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaSb_plot_main_GaSb_Adachi.py` (``../gallery/GaSb/plot_main_GaSb_Adachi.py``)
     - 00:00.617
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cu_plot_main_Cu_Werner.py` (``../gallery/Cu/plot_main_Cu_Werner.py``)
     - 00:00.617
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_InSb_plot_main_InSb_Aspnes.py` (``../gallery/InSb/plot_main_InSb_Aspnes.py``)
     - 00:00.617
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaAs_plot_main_GaAs_Aspnes.py` (``../gallery/GaAs/plot_main_GaAs_Aspnes.py``)
     - 00:00.617
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Islam_o.py` (``../gallery/MoS2/plot_main_MoS2_Islam_o.py``)
     - 00:00.617
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KNbO3_plot_main_KNbO3_Umemura_alpha.py` (``../gallery/KNbO3/plot_main_KNbO3_Umemura_alpha.py``)
     - 00:00.617
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO2_plot_main_SiO2_Nyakuchena.py` (``../gallery/SiO2/plot_main_SiO2_Nyakuchena.py``)
     - 00:00.617
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ir_plot_main_Ir_Hass_300C.py` (``../gallery/Ir/plot_main_Ir_Hass_300C.py``)
     - 00:00.617
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_In_plot_main_In_Mathewson_298K.py` (``../gallery/In/plot_main_In_Mathewson_298K.py``)
     - 00:00.617
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Sinnock_liquid_115.95K.py` (``../gallery/Kr/plot_main_Kr_Sinnock_liquid_115.95K.py``)
     - 00:00.616
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Li_200K.py` (``../gallery/Ge/plot_main_Ge_Li_200K.py``)
     - 00:00.616
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_HgGa2S4_plot_main_HgGa2S4_Kato_o.py` (``../gallery/HgGa2S4/plot_main_HgGa2S4_Kato_o.py``)
     - 00:00.616
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_TiN_plot_main_TiN_Schnabel.py` (``../gallery/TiN/plot_main_TiN_Schnabel.py``)
     - 00:00.616
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Hg_plot_main_Hg_Inagaki.py` (``../gallery/Hg/plot_main_Hg_Inagaki.py``)
     - 00:00.616
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_HfO2_plot_main_HfO2_Al_Kuhaili.py` (``../gallery/HfO2/plot_main_HfO2_Al_Kuhaili.py``)
     - 00:00.615
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pb5Ge3O11_plot_main_Pb5Ge3O11_Simon_e.py` (``../gallery/Pb5Ge3O11/plot_main_Pb5Ge3O11_Simon_e.py``)
     - 00:00.615
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Rowe_273K.py` (``../gallery/H2O/plot_main_H2O_Rowe_273K.py``)
     - 00:00.615
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Mo_plot_main_Mo_Werner.py` (``../gallery/Mo/plot_main_Mo_Werner.py``)
     - 00:00.615
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_200K.py` (``../gallery/Si/plot_main_Si_Li_200K.py``)
     - 00:00.615
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Ermolaev_o.py` (``../gallery/MoS2/plot_main_MoS2_Ermolaev_o.py``)
     - 00:00.614
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Hsu_2L.py` (``../gallery/MoS2/plot_main_MoS2_Hsu_2L.py``)
     - 00:00.614
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_N2_plot_main_N2_Griesmann.py` (``../gallery/N2/plot_main_N2_Griesmann.py``)
     - 00:00.614
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Nb_plot_main_Nb_Weaver.py` (``../gallery/Nb/plot_main_Nb_Weaver.py``)
     - 00:00.614
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KNbO3_plot_main_KNbO3_Zysset_gamma.py` (``../gallery/KNbO3/plot_main_KNbO3_Zysset_gamma.py``)
     - 00:00.613
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Green_2008.py` (``../gallery/Si/plot_main_Si_Green_2008.py``)
     - 00:00.613
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_HfSe2_plot_main_HfSe2_Zotev_e.py` (``../gallery/HfSe2/plot_main_HfSe2_Zotev_e.py``)
     - 00:00.613
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Song_sHOPG_o.py` (``../gallery/C/plot_main_C_Song_sHOPG_o.py``)
     - 00:00.613
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PbS_plot_main_PbS_Zemel.py` (``../gallery/PbS/plot_main_PbS_Zemel.py``)
     - 00:00.613
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Daimon_20.0C.py` (``../gallery/H2O/plot_main_H2O_Daimon_20.0C.py``)
     - 00:00.613
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Wang.py` (``../gallery/H2O/plot_main_H2O_Wang.py``)
     - 00:00.612
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_K_plot_main_K_Whang.py` (``../gallery/K/plot_main_K_Whang.py``)
     - 00:00.612
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaAs_plot_main_GaAs_Skauli.py` (``../gallery/GaAs/plot_main_GaAs_Skauli.py``)
     - 00:00.612
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Mn_plot_main_Mn_Johnson.py` (``../gallery/Mn/plot_main_Mn_Johnson.py``)
     - 00:00.612
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pt_plot_main_Pt_Rakic_BB.py` (``../gallery/Pt/plot_main_Pt_Rakic_BB.py``)
     - 00:00.612
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ho_plot_main_Ho_Fernandez_Perea.py` (``../gallery/Ho/plot_main_Ho_Fernandez_Perea.py``)
     - 00:00.612
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgF2_plot_main_MgF2_Zheng_o_21C.py` (``../gallery/MgF2/plot_main_MgF2_Zheng_o_21C.py``)
     - 00:00.612
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cu_plot_main_Cu_Johnson.py` (``../gallery/Cu/plot_main_Cu_Johnson.py``)
     - 00:00.611
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PbSe_plot_main_PbSe_Suzuki.py` (``../gallery/PbSe/plot_main_PbSe_Suzuki.py``)
     - 00:00.611
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaN_plot_main_GaN_Kawashima.py` (``../gallery/GaN/plot_main_GaN_Kawashima.py``)
     - 00:00.611
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KTiOPO4_plot_main_KTiOPO4_Kato_gamma.py` (``../gallery/KTiOPO4/plot_main_KTiOPO4_Kato_gamma.py``)
     - 00:00.611
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaAs_plot_main_GaAs_Ozaki.py` (``../gallery/GaAs/plot_main_GaAs_Ozaki.py``)
     - 00:00.611
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Li_550K.py` (``../gallery/Ge/plot_main_Ge_Li_550K.py``)
     - 00:00.611
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoO3_plot_main_MoO3_Lajaunie_gamma.py` (``../gallery/MoO3/plot_main_MoO3_Lajaunie_gamma.py``)
     - 00:00.610
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Ciesielski_2nm.py` (``../gallery/Ge/plot_main_Ge_Ciesielski_2nm.py``)
     - 00:00.610
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pb_plot_main_Pb_Golovashkin_4.2K.py` (``../gallery/Pb/plot_main_Pb_Golovashkin_4.2K.py``)
     - 00:00.610
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Daimon_19.0C.py` (``../gallery/H2O/plot_main_H2O_Daimon_19.0C.py``)
     - 00:00.610
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Vuye_200C.py` (``../gallery/Si/plot_main_Si_Vuye_200C.py``)
     - 00:00.610
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_NaCl_plot_main_NaCl_Li.py` (``../gallery/NaCl/plot_main_NaCl_Li.py``)
     - 00:00.610
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoSe2_plot_main_MoSe2_Hsu_3L.py` (``../gallery/MoSe2/plot_main_MoSe2_Hsu_3L.py``)
     - 00:00.610
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CsCl_plot_main_CsCl_Li.py` (``../gallery/CsCl/plot_main_CsCl_Li.py``)
     - 00:00.609
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge2Sb2Te5_plot_main_Ge2Sb2Te5_Frantz_crystal.py` (``../gallery/Ge2Sb2Te5/plot_main_Ge2Sb2Te5_Frantz_crystal.py``)
     - 00:00.609
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_NaCl_plot_main_NaCl_Querry.py` (``../gallery/NaCl/plot_main_NaCl_Querry.py``)
     - 00:00.609
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pb_plot_main_Pb_Ordal.py` (``../gallery/Pb/plot_main_Pb_Ordal.py``)
     - 00:00.609
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Munkhbat_o.py` (``../gallery/MoS2/plot_main_MoS2_Munkhbat_o.py``)
     - 00:00.608
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Song_13L.py` (``../gallery/MoS2/plot_main_MoS2_Song_13L.py``)
     - 00:00.608
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Te_plot_main_Te_Guo_o.py` (``../gallery/Te/plot_main_Te_Guo_o.py``)
     - 00:00.608
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Nunley.py` (``../gallery/Ge/plot_main_Ge_Nunley.py``)
     - 00:00.608
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KNbO3_plot_main_KNbO3_Zysset_beta.py` (``../gallery/KNbO3/plot_main_KNbO3_Zysset_beta.py``)
     - 00:00.608
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Bideau_Mehu.py` (``../gallery/Kr/plot_main_Kr_Bideau_Mehu.py``)
     - 00:00.608
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Fe2O3_plot_main_Fe2O3_Querry_e.py` (``../gallery/Fe2O3/plot_main_Fe2O3_Querry_e.py``)
     - 00:00.608
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CuO_plot_main_CuO_Brimhall.py` (``../gallery/CuO/plot_main_CuO_Brimhall.py``)
     - 00:00.608
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Mn_plot_main_Mn_Querry.py` (``../gallery/Mn/plot_main_Mn_Querry.py``)
     - 00:00.607
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KBr_plot_main_KBr_Li.py` (``../gallery/KBr/plot_main_KBr_Li.py``)
     - 00:00.607
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Li_293K.py` (``../gallery/Ge/plot_main_Ge_Li_293K.py``)
     - 00:00.607
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Hsu_2L.py` (``../gallery/WSe2/plot_main_WSe2_Hsu_2L.py``)
     - 00:00.607
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaN_plot_main_GaN_Barker_o.py` (``../gallery/GaN/plot_main_GaN_Barker_o.py``)
     - 00:00.607
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Salzberg.py` (``../gallery/Si/plot_main_Si_Salzberg.py``)
     - 00:00.607
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoO3_plot_main_MoO3_Lajaunie_alpha.py` (``../gallery/MoO3/plot_main_MoO3_Lajaunie_alpha.py``)
     - 00:00.606
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaAs_plot_main_GaAs_Papatryfonos.py` (``../gallery/GaAs/plot_main_GaAs_Papatryfonos.py``)
     - 00:00.606
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KF_plot_main_KF_Li.py` (``../gallery/KF/plot_main_KF_Li.py``)
     - 00:00.606
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_InAs_plot_main_InAs_Lorimor.py` (``../gallery/InAs/plot_main_InAs_Lorimor.py``)
     - 00:00.606
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_NbSe2_plot_main_NbSe2_Munkhbat_o.py` (``../gallery/NbSe2/plot_main_NbSe2_Munkhbat_o.py``)
     - 00:00.605
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cu_plot_main_Cu_Querry.py` (``../gallery/Cu/plot_main_Cu_Querry.py``)
     - 00:00.605
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_He_plot_main_He_Borzsonyi.py` (``../gallery/He/plot_main_He_Borzsonyi.py``)
     - 00:00.605
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_550K.py` (``../gallery/Si/plot_main_Si_Li_550K.py``)
     - 00:00.605
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ni_plot_main_Ni_Ordal.py` (``../gallery/Ni/plot_main_Ni_Ordal.py``)
     - 00:00.604
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_RbBr_plot_main_RbBr_Li.py` (``../gallery/RbBr/plot_main_RbBr_Li.py``)
     - 00:00.604
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Ermolaev_1L.py` (``../gallery/MoS2/plot_main_MoS2_Ermolaev_1L.py``)
     - 00:00.604
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_N2_plot_main_N2_Peck_15C.py` (``../gallery/N2/plot_main_N2_Peck_15C.py``)
     - 00:00.604
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_D2O_plot_main_D2O_Asfar.py` (``../gallery/D2O/plot_main_D2O_Asfar.py``)
     - 00:00.604
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_NbSe2_plot_main_NbSe2_Munkhbat_e.py` (``../gallery/NbSe2/plot_main_NbSe2_Munkhbat_e.py``)
     - 00:00.603
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Kofman_70K.py` (``../gallery/H2O/plot_main_H2O_Kofman_70K.py``)
     - 00:00.603
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KNbO3_plot_main_KNbO3_Umemura_beta.py` (``../gallery/KNbO3/plot_main_KNbO3_Umemura_beta.py``)
     - 00:00.602
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaAs_plot_main_GaAs_Perner.py` (``../gallery/GaAs/plot_main_GaAs_Perner.py``)
     - 00:00.602
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Fe_plot_main_Fe_Werner_DFT.py` (``../gallery/Fe/plot_main_Fe_Werner_DFT.py``)
     - 00:00.602
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_HgS_plot_main_HgS_Bond_e.py` (``../gallery/HgS/plot_main_HgS_Bond_e.py``)
     - 00:00.602
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Fe_plot_main_Fe_Querry.py` (``../gallery/Fe/plot_main_Fe_Querry.py``)
     - 00:00.602
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_InAs_plot_main_InAs_Adachi.py` (``../gallery/InAs/plot_main_InAs_Adachi.py``)
     - 00:00.602
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaN_plot_main_GaN_Lin_zincblende.py` (``../gallery/GaN/plot_main_GaN_Lin_zincblende.py``)
     - 00:00.602
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaSb_plot_main_GaSb_Djurisic.py` (``../gallery/GaSb/plot_main_GaSb_Djurisic.py``)
     - 00:00.602
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Te_plot_main_Te_Sherman_o.py` (``../gallery/Te/plot_main_Te_Sherman_o.py``)
     - 00:00.602
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Grace_liquid_178K.py` (``../gallery/Xe/plot_main_Xe_Grace_liquid_178K.py``)
     - 00:00.601
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Kofman_90K.py` (``../gallery/H2O/plot_main_H2O_Kofman_90K.py``)
     - 00:00.601
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Li_100K.py` (``../gallery/Ge/plot_main_Ge_Li_100K.py``)
     - 00:00.601
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Daimon_24.0C.py` (``../gallery/H2O/plot_main_H2O_Daimon_24.0C.py``)
     - 00:00.601
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ir_plot_main_Ir_Hass_40C.py` (``../gallery/Ir/plot_main_Ir_Hass_40C.py``)
     - 00:00.600
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_K_plot_main_K_Althoff.py` (``../gallery/K/plot_main_K_Althoff.py``)
     - 00:00.600
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ni_plot_main_Ni_Rakic_LD.py` (``../gallery/Ni/plot_main_Ni_Rakic_LD.py``)
     - 00:00.600
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Song_6L.py` (``../gallery/MoS2/plot_main_MoS2_Song_6L.py``)
     - 00:00.600
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgO_plot_main_MgO_Stephens_vis.py` (``../gallery/MgO/plot_main_MgO_Stephens_vis.py``)
     - 00:00.600
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CeF3_plot_main_CeF3_Rodriguez_de_Marcos.py` (``../gallery/CeF3/plot_main_CeF3_Rodriguez_de_Marcos.py``)
     - 00:00.600
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Mo_plot_main_Mo_Ordal.py` (``../gallery/Mo/plot_main_Mo_Ordal.py``)
     - 00:00.600
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Song_3L.py` (``../gallery/MoS2/plot_main_MoS2_Song_3L.py``)
     - 00:00.599
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2_plot_main_H2_Smith.py` (``../gallery/H2/plot_main_H2_Smith.py``)
     - 00:00.599
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_InSb_plot_main_InSb_Adachi.py` (``../gallery/InSb/plot_main_InSb_Adachi.py``)
     - 00:00.599
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Rowe_263K.py` (``../gallery/H2O/plot_main_H2O_Rowe_263K.py``)
     - 00:00.599
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Li_450K.py` (``../gallery/Ge/plot_main_Ge_Li_450K.py``)
     - 00:00.599
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_EuS_plot_main_EuS_Meretska.py` (``../gallery/EuS/plot_main_EuS_Meretska.py``)
     - 00:00.599
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_He_plot_main_He_Ermolov.py` (``../gallery/He/plot_main_He_Ermolov.py``)
     - 00:00.599
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_He_plot_main_He_Smith.py` (``../gallery/He/plot_main_He_Smith.py``)
     - 00:00.599
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ne_plot_main_Ne_Borzsonyi.py` (``../gallery/Ne/plot_main_Ne_Borzsonyi.py``)
     - 00:00.598
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdTe_plot_main_CdTe_DeBell_300K.py` (``../gallery/CdTe/plot_main_CdTe_DeBell_300K.py``)
     - 00:00.598
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2_plot_main_H2_Peck.py` (``../gallery/H2/plot_main_H2_Peck.py``)
     - 00:00.598
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Icenogle.py` (``../gallery/Ge/plot_main_Ge_Icenogle.py``)
     - 00:00.598
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdTe_plot_main_CdTe_Marple.py` (``../gallery/CdTe/plot_main_CdTe_Marple.py``)
     - 00:00.598
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KCl_plot_main_KCl_Querry.py` (``../gallery/KCl/plot_main_KCl_Querry.py``)
     - 00:00.598
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Fe_plot_main_Fe_Werner.py` (``../gallery/Fe/plot_main_Fe_Werner.py``)
     - 00:00.597
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PbF2_plot_main_PbF2_Malitson.py` (``../gallery/PbF2/plot_main_PbF2_Malitson.py``)
     - 00:00.597
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ir_plot_main_Ir_Schmitt_MS.py` (``../gallery/Ir/plot_main_Ir_Schmitt_MS.py``)
     - 00:00.597
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Kofman_150K.py` (``../gallery/H2O/plot_main_H2O_Kofman_150K.py``)
     - 00:00.597
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CsI_plot_main_CsI_Querry.py` (``../gallery/CsI/plot_main_CsI_Querry.py``)
     - 00:00.596
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiO2_plot_main_SiO2_Kischkat.py` (``../gallery/SiO2/plot_main_SiO2_Kischkat.py``)
     - 00:00.596
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_HgS_plot_main_HgS_Bond_o.py` (``../gallery/HgS/plot_main_HgS_Bond_o.py``)
     - 00:00.595
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_HgGa2S4_plot_main_HgGa2S4_Kato_e.py` (``../gallery/HgGa2S4/plot_main_HgGa2S4_Kato_e.py``)
     - 00:00.595
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_InP_plot_main_InP_Pettit.py` (``../gallery/InP/plot_main_InP_Pettit.py``)
     - 00:00.594
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_HfSe2_plot_main_HfSe2_Zotev_o.py` (``../gallery/HfSe2/plot_main_HfSe2_Zotev_o.py``)
     - 00:00.593
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Kofman_110K.py` (``../gallery/H2O/plot_main_H2O_Kofman_110K.py``)
     - 00:00.593
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Sinnock_solid_95K.py` (``../gallery/Kr/plot_main_Kr_Sinnock_solid_95K.py``)
     - 00:00.593
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaB2O4_plot_main_BaB2O4_Zhang_o.py` (``../gallery/BaB2O4/plot_main_BaB2O4_Zhang_o.py``)
     - 00:00.593
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Arakawa.py` (``../gallery/C/plot_main_C_Arakawa.py``)
     - 00:00.592
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdTe_plot_main_CdTe_DeBell_80K.py` (``../gallery/CdTe/plot_main_CdTe_DeBell_80K.py``)
     - 00:00.592
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_InP_plot_main_InP_Panah.py` (``../gallery/InP/plot_main_InP_Panah.py``)
     - 00:00.592
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Dy2O3_plot_main_Dy2O3_Medenbach.py` (``../gallery/Dy2O3/plot_main_Dy2O3_Medenbach.py``)
     - 00:00.592
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdS_plot_main_CdS_Ninomiya_e.py` (``../gallery/CdS/plot_main_CdS_Ninomiya_e.py``)
     - 00:00.592
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdSe_plot_main_CdSe_Ninomiya_cubic.py` (``../gallery/CdSe/plot_main_CdSe_Ninomiya_cubic.py``)
     - 00:00.592
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Mo_plot_main_Mo_Windt.py` (``../gallery/Mo/plot_main_Mo_Windt.py``)
     - 00:00.591
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_In2Se3_plot_main_In2Se3_Zotev.py` (``../gallery/In2Se3/plot_main_In2Se3_Zotev.py``)
     - 00:00.591
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Beal.py` (``../gallery/MoS2/plot_main_MoS2_Beal.py``)
     - 00:00.591
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ca_plot_main_Ca_Rodriguez_de_Marcos.py` (``../gallery/Ca/plot_main_Ca_Rodriguez_de_Marcos.py``)
     - 00:00.591
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Querry_Asbury3222.py` (``../gallery/C/plot_main_C_Querry_Asbury3222.py``)
     - 00:00.590
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgF2_plot_main_MgF2_Dodge_e.py` (``../gallery/MgF2/plot_main_MgF2_Dodge_e.py``)
     - 00:00.590
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Daimon_21.5C.py` (``../gallery/H2O/plot_main_H2O_Daimon_21.5C.py``)
     - 00:00.590
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Bi2Se3_plot_main_Bi2Se3_Fang_6L.py` (``../gallery/Bi2Se3/plot_main_Bi2Se3_Fang_6L.py``)
     - 00:00.590
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Be_plot_main_Be_Svechnikov.py` (``../gallery/Be/plot_main_Be_Svechnikov.py``)
     - 00:00.589
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CsBr_plot_main_CsBr_Rodney.py` (``../gallery/CsBr/plot_main_CsBr_Rodney.py``)
     - 00:00.589
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Vuye_450C.py` (``../gallery/Si/plot_main_Si_Vuye_450C.py``)
     - 00:00.589
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_D2O_plot_main_D2O_Sarkar.py` (``../gallery/D2O/plot_main_D2O_Sarkar.py``)
     - 00:00.589
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Kofman_10K.py` (``../gallery/H2O/plot_main_H2O_Kofman_10K.py``)
     - 00:00.589
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Querry_DixonHPN2.py` (``../gallery/C/plot_main_C_Querry_DixonHPN2.py``)
     - 00:00.588
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CO2_plot_main_CO2_Old.py` (``../gallery/CO2/plot_main_CO2_Old.py``)
     - 00:00.588
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaAs_plot_main_GaAs_Kachare.py` (``../gallery/GaAs/plot_main_GaAs_Kachare.py``)
     - 00:00.588
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Ermolaev.py` (``../gallery/C/plot_main_C_Ermolaev.py``)
     - 00:00.588
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdSe_plot_main_CdSe_Bond_o.py` (``../gallery/CdSe/plot_main_CdSe_Bond_o.py``)
     - 00:00.587
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Fe3O4_plot_main_Fe3O4_Querry.py` (``../gallery/Fe3O4/plot_main_Fe3O4_Querry.py``)
     - 00:00.587
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Cuthbertson.py` (``../gallery/Xe/plot_main_Xe_Cuthbertson.py``)
     - 00:00.587
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgF2_plot_main_MgF2_Zheng_e_368C.py` (``../gallery/MgF2/plot_main_MgF2_Zheng_e_368C.py``)
     - 00:00.587
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_HfO2_plot_main_HfO2_Bright.py` (``../gallery/HfO2/plot_main_HfO2_Bright.py``)
     - 00:00.587
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaP_plot_main_GaP_Aspnes.py` (``../gallery/GaP/plot_main_GaP_Aspnes.py``)
     - 00:00.587
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Te_plot_main_Te_Caldwell_o.py` (``../gallery/Te/plot_main_Te_Caldwell_o.py``)
     - 00:00.586
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgF2_plot_main_MgF2_Zheng_o_186C.py` (``../gallery/MgF2/plot_main_MgF2_Zheng_o_186C.py``)
     - 00:00.586
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CsBr_plot_main_CsBr_Querry.py` (``../gallery/CsBr/plot_main_CsBr_Querry.py``)
     - 00:00.586
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SiC_plot_main_SiC_Shaffer.py` (``../gallery/SiC/plot_main_SiC_Shaffer.py``)
     - 00:00.585
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Ermolaev_SWCNT_e.py` (``../gallery/C/plot_main_C_Ermolaev_SWCNT_e.py``)
     - 00:00.585
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Rb_plot_main_Rb_Ives.py` (``../gallery/Rb/plot_main_Rb_Ives.py``)
     - 00:00.585
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Kofman_130K.py` (``../gallery/H2O/plot_main_H2O_Kofman_130K.py``)
     - 00:00.585
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgH2_plot_main_MgH2_Palm.py` (``../gallery/MgH2/plot_main_MgH2_Palm.py``)
     - 00:00.585
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CO1_plot_main_CO1_Smith.py` (``../gallery/CO1/plot_main_CO1_Smith.py``)
     - 00:00.584
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cr_plot_main_Cr_Rakic_LD.py` (``../gallery/Cr/plot_main_Cr_Rakic_LD.py``)
     - 00:00.584
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaN_plot_main_GaN_Barker_e.py` (``../gallery/GaN/plot_main_GaN_Barker_e.py``)
     - 00:00.584
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CsLiB6O10_plot_main_CsLiB6O10_Sasaki_o.py` (``../gallery/CsLiB6O10/plot_main_CsLiB6O10_Sasaki_o.py``)
     - 00:00.584
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdSe_plot_main_CdSe_Ninomiya_e.py` (``../gallery/CdSe/plot_main_CdSe_Ninomiya_e.py``)
     - 00:00.584
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cs_plot_main_Cs_Monin.py` (``../gallery/Cs/plot_main_Cs_Monin.py``)
     - 00:00.583
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_K_plot_main_K_Smith.py` (``../gallery/K/plot_main_K_Smith.py``)
     - 00:00.583
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cs_plot_main_Cs_Ives.py` (``../gallery/Cs/plot_main_Cs_Ives.py``)
     - 00:00.583
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ce_plot_main_Ce_Fernandez_Perea.py` (``../gallery/Ce/plot_main_Ce_Fernandez_Perea.py``)
     - 00:00.583
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2_plot_main_H2_Koch.py` (``../gallery/H2/plot_main_H2_Koch.py``)
     - 00:00.583
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Zotev_e.py` (``../gallery/WSe2/plot_main_WSe2_Zotev_e.py``)
     - 00:00.582
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CsLiB6O10_plot_main_CsLiB6O10_Sasaki_e.py` (``../gallery/CsLiB6O10/plot_main_CsLiB6O10_Sasaki_e.py``)
     - 00:00.582
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_In_plot_main_In_Golovashkin_4.2K.py` (``../gallery/In/plot_main_In_Golovashkin_4.2K.py``)
     - 00:00.582
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaMg_CO3_2_plot_main_CaMg_CO3_2_Querry_o.py` (``../gallery/CaMg_CO3_2/plot_main_CaMg_CO3_2_Querry_o.py``)
     - 00:00.582
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaF2_plot_main_CaF2_Kaiser.py` (``../gallery/CaF2/plot_main_CaF2_Kaiser.py``)
     - 00:00.582
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Burnett.py` (``../gallery/Ge/plot_main_Ge_Burnett.py``)
     - 00:00.581
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaS_plot_main_GaS_Kato_e.py` (``../gallery/GaS/plot_main_GaS_Kato_e.py``)
     - 00:00.581
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_As2S3_plot_main_As2S3_Slavich_abinitio_alpha.py` (``../gallery/As2S3/plot_main_As2S3_Slavich_abinitio_alpha.py``)
     - 00:00.581
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cr_plot_main_Cr_Johnson.py` (``../gallery/Cr/plot_main_Cr_Johnson.py``)
     - 00:00.580
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaF2_plot_main_CaF2_Zheng_368C.py` (``../gallery/CaF2/plot_main_CaF2_Zheng_368C.py``)
     - 00:00.580
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cu_plot_main_Cu_Babar.py` (``../gallery/Cu/plot_main_Cu_Babar.py``)
     - 00:00.580
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_D2_plot_main_D2_Weber.py` (``../gallery/D2/plot_main_D2_Weber.py``)
     - 00:00.580
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cu_plot_main_Cu_Stahrenberg.py` (``../gallery/Cu/plot_main_Cu_Stahrenberg.py``)
     - 00:00.579
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaMoO4_plot_main_CaMoO4_Bond_o.py` (``../gallery/CaMoO4/plot_main_CaMoO4_Bond_o.py``)
     - 00:00.579
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaYAlO4_plot_main_CaYAlO4_Loiko_e.py` (``../gallery/CaYAlO4/plot_main_CaYAlO4_Loiko_e.py``)
     - 00:00.579
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Bi2Se3_plot_main_Bi2Se3_Fang_4L.py` (``../gallery/Bi2Se3/plot_main_Bi2Se3_Fang_4L.py``)
     - 00:00.579
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Bi4Ge3O12_plot_main_Bi4Ge3O12_Williams.py` (``../gallery/Bi4Ge3O12/plot_main_Bi4Ge3O12_Williams.py``)
     - 00:00.579
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdSe_plot_main_CdSe_Ninomiya_o.py` (``../gallery/CdSe/plot_main_CdSe_Ninomiya_o.py``)
     - 00:00.579
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cu2O_plot_main_Cu2O_Querry.py` (``../gallery/Cu2O/plot_main_Cu2O_Querry.py``)
     - 00:00.579
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaGdAlO4_plot_main_CaGdAlO4_Loiko_o.py` (``../gallery/CaGdAlO4/plot_main_CaGdAlO4_Loiko_o.py``)
     - 00:00.579
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ir_plot_main_Ir_Schmitt_ALD.py` (``../gallery/Ir/plot_main_Ir_Schmitt_ALD.py``)
     - 00:00.578
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_B_plot_main_B_Fernandez_Perea.py` (``../gallery/B/plot_main_B_Fernandez_Perea.py``)
     - 00:00.578
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BN_plot_main_BN_Lee.py` (``../gallery/BN/plot_main_BN_Lee.py``)
     - 00:00.577
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdGa2S4_plot_main_CdGa2S4_Kato_e.py` (``../gallery/CdGa2S4/plot_main_CdGa2S4_Kato_e.py``)
     - 00:00.577
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Bi_plot_main_Bi_Werner.py` (``../gallery/Bi/plot_main_Bi_Werner.py``)
     - 00:00.577
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Peter.py` (``../gallery/C/plot_main_C_Peter.py``)
     - 00:00.577
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YVO4_plot_main_YVO4_Shi_o_80C.py` (``../gallery/YVO4/plot_main_YVO4_Shi_o_80C.py``)
     - 00:00.577
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cu_plot_main_Cu_Rakic_LD.py` (``../gallery/Cu/plot_main_Cu_Rakic_LD.py``)
     - 00:00.577
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Co_plot_main_Co_Johnson.py` (``../gallery/Co/plot_main_Co_Johnson.py``)
     - 00:00.577
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdS_plot_main_CdS_Treharne.py` (``../gallery/CdS/plot_main_CdS_Treharne.py``)
     - 00:00.577
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoO3_plot_main_MoO3_Lajaunie_beta.py` (``../gallery/MoO3/plot_main_MoO3_Lajaunie_beta.py``)
     - 00:00.576
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CsI_plot_main_CsI_Li.py` (``../gallery/CsI/plot_main_CsI_Li.py``)
     - 00:00.576
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_250K.py` (``../gallery/Si/plot_main_Si_Li_250K.py``)
     - 00:00.576
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Djurisic_e.py` (``../gallery/C/plot_main_C_Djurisic_e.py``)
     - 00:00.576
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaTiO3_plot_main_BaTiO3_Wemple_o.py` (``../gallery/BaTiO3/plot_main_BaTiO3_Wemple_o.py``)
     - 00:00.575
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ca_plot_main_Ca_Mathewson.py` (``../gallery/Ca/plot_main_Ca_Mathewson.py``)
     - 00:00.575
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Bi2Se3_plot_main_Bi2Se3_Fang_2L.py` (``../gallery/Bi2Se3/plot_main_Bi2Se3_Fang_2L.py``)
     - 00:00.575
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cr_plot_main_Cr_Sytchkova.py` (``../gallery/Cr/plot_main_Cr_Sytchkova.py``)
     - 00:00.575
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GeO2_plot_main_GeO2_Nunley.py` (``../gallery/GeO2/plot_main_GeO2_Nunley.py``)
     - 00:00.575
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaCO3_plot_main_CaCO3_Ghosh_o.py` (``../gallery/CaCO3/plot_main_CaCO3_Ghosh_o.py``)
     - 00:00.575
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaCO3_plot_main_CaCO3_Ghosh_e.py` (``../gallery/CaCO3/plot_main_CaCO3_Ghosh_e.py``)
     - 00:00.575
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Rosenblatt_21nm.py` (``../gallery/Au/plot_main_Au_Rosenblatt_21nm.py``)
     - 00:00.575
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BN_plot_main_BN_Grudinin_o.py` (``../gallery/BN/plot_main_BN_Grudinin_o.py``)
     - 00:00.575
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa4S7_plot_main_BaGa4S7_Kato_alpha.py` (``../gallery/BaGa4S7/plot_main_BaGa4S7_Kato_alpha.py``)
     - 00:00.574
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CuGaS2_plot_main_CuGaS2_Boyd_120_o.py` (``../gallery/CuGaS2/plot_main_CuGaS2_Boyd_120_o.py``)
     - 00:00.574
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KTiOPO4_plot_main_KTiOPO4_Kato_beta.py` (``../gallery/KTiOPO4/plot_main_KTiOPO4_Kato_beta.py``)
     - 00:00.574
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_InP_plot_main_InP_Adachi.py` (``../gallery/InP/plot_main_InP_Adachi.py``)
     - 00:00.574
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaF2_plot_main_CaF2_Zheng_21C.py` (``../gallery/CaF2/plot_main_CaF2_Zheng_21C.py``)
     - 00:00.574
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdGeP2_plot_main_CdGeP2_Boyd_118_e.py` (``../gallery/CdGeP2/plot_main_CdGeP2_Boyd_118_e.py``)
     - 00:00.574
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdGa2S4_plot_main_CdGa2S4_Kato_o.py` (``../gallery/CdGa2S4/plot_main_CdGa2S4_Kato_o.py``)
     - 00:00.574
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaWO4_plot_main_CaWO4_Bond_o.py` (``../gallery/CaWO4/plot_main_CaWO4_Bond_o.py``)
     - 00:00.574
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdSe_plot_main_CdSe_Lisitsa_o.py` (``../gallery/CdSe/plot_main_CdSe_Lisitsa_o.py``)
     - 00:00.574
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_El_Sayed.py` (``../gallery/C/plot_main_C_El_Sayed.py``)
     - 00:00.574
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaWO4_plot_main_CaWO4_Bond_e.py` (``../gallery/CaWO4/plot_main_CaWO4_Bond_e.py``)
     - 00:00.573
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgF2_plot_main_MgF2_Dodge_o.py` (``../gallery/MgF2/plot_main_MgF2_Dodge_o.py``)
     - 00:00.573
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaF2_plot_main_CaF2_Zheng_186C.py` (``../gallery/CaF2/plot_main_CaF2_Zheng_186C.py``)
     - 00:00.573
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaF2_plot_main_BaF2_Zheng_186C.py` (``../gallery/BaF2/plot_main_BaF2_Zheng_186C.py``)
     - 00:00.573
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Taylor.py` (``../gallery/C/plot_main_C_Taylor.py``)
     - 00:00.573
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaMg_CO3_2_plot_main_CaMg_CO3_2_Querry_e.py` (``../gallery/CaMg_CO3_2/plot_main_CaMg_CO3_2_Querry_e.py``)
     - 00:00.573
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaF2_plot_main_BaF2_Kaiser.py` (``../gallery/BaF2/plot_main_BaF2_Kaiser.py``)
     - 00:00.573
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaS_plot_main_GaS_Zotev_e.py` (``../gallery/GaS/plot_main_GaS_Zotev_e.py``)
     - 00:00.572
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_D2O_plot_main_D2O_Wang.py` (``../gallery/D2O/plot_main_D2O_Wang.py``)
     - 00:00.572
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Bi2Se3_plot_main_Bi2Se3_Fang_10L.py` (``../gallery/Bi2Se3/plot_main_Bi2Se3_Fang_10L.py``)
     - 00:00.572
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KTaO3_plot_main_KTaO3_Fujii.py` (``../gallery/KTaO3/plot_main_KTaO3_Fujii.py``)
     - 00:00.572
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Co_plot_main_Co_Werner_DFT.py` (``../gallery/Co/plot_main_Co_Werner_DFT.py``)
     - 00:00.572
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Amotchkina.py` (``../gallery/Ge/plot_main_Ge_Amotchkina.py``)
     - 00:00.571
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Kedenburg.py` (``../gallery/H2O/plot_main_H2O_Kedenburg.py``)
     - 00:00.571
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdGeAs2_plot_main_CdGeAs2_Boyd_e.py` (``../gallery/CdGeAs2/plot_main_CdGeAs2_Boyd_e.py``)
     - 00:00.570
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaF2_plot_main_CaF2_Li.py` (``../gallery/CaF2/plot_main_CaF2_Li.py``)
     - 00:00.570
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Li_350K.py` (``../gallery/Ge/plot_main_Ge_Li_350K.py``)
     - 00:00.570
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdGeAs2_plot_main_CdGeAs2_Boyd_o.py` (``../gallery/CdGeAs2/plot_main_CdGeAs2_Boyd_o.py``)
     - 00:00.570
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CS2_plot_main_CS2_Chemnitz.py` (``../gallery/CS2/plot_main_CS2_Chemnitz.py``)
     - 00:00.570
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_InP_plot_main_InP_Aspnes.py` (``../gallery/InP/plot_main_InP_Aspnes.py``)
     - 00:00.569
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaP_plot_main_GaP_Bond.py` (``../gallery/GaP/plot_main_GaP_Bond.py``)
     - 00:00.569
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GeO2_plot_main_GeO2_Fleming.py` (``../gallery/GeO2/plot_main_GeO2_Fleming.py``)
     - 00:00.569
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_700K.py` (``../gallery/Si/plot_main_Si_Li_700K.py``)
     - 00:00.568
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaAs_plot_main_GaAs_Adachi.py` (``../gallery/GaAs/plot_main_GaAs_Adachi.py``)
     - 00:00.568
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Aspnes.py` (``../gallery/Ge/plot_main_Ge_Aspnes.py``)
     - 00:00.568
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cu_plot_main_Cu_McPeak.py` (``../gallery/Cu/plot_main_Cu_McPeak.py``)
     - 00:00.568
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BiB3O6_plot_main_BiB3O6_Umemura_gamma.py` (``../gallery/BiB3O6/plot_main_BiB3O6_Umemura_gamma.py``)
     - 00:00.568
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Song_cHOPG_e.py` (``../gallery/C/plot_main_C_Song_cHOPG_e.py``)
     - 00:00.567
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaF2_plot_main_BaF2_Li.py` (``../gallery/BaF2/plot_main_BaF2_Li.py``)
     - 00:00.567
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cr_plot_main_Cr_Rakic_BB.py` (``../gallery/Cr/plot_main_Cr_Rakic_BB.py``)
     - 00:00.567
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Weber.py` (``../gallery/C/plot_main_C_Weber.py``)
     - 00:00.567
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaF2_plot_main_BaF2_Zheng_368C.py` (``../gallery/BaF2/plot_main_BaF2_Zheng_368C.py``)
     - 00:00.567
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cs_plot_main_Cs_Whang.py` (``../gallery/Cs/plot_main_Cs_Whang.py``)
     - 00:00.567
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CO2_plot_main_CO2_Bideau_Mehu.py` (``../gallery/CO2/plot_main_CO2_Bideau_Mehu.py``)
     - 00:00.567
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdSe_plot_main_CdSe_Bond_e.py` (``../gallery/CdSe/plot_main_CdSe_Bond_e.py``)
     - 00:00.567
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BeAl2O4_plot_main_BeAl2O4_Walling_alpha.py` (``../gallery/BeAl2O4/plot_main_BeAl2O4_Walling_alpha.py``)
     - 00:00.567
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Fe_plot_main_Fe_Johnson.py` (``../gallery/Fe/plot_main_Fe_Johnson.py``)
     - 00:00.566
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CsBr_plot_main_CsBr_Li.py` (``../gallery/CsBr/plot_main_CsBr_Li.py``)
     - 00:00.566
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdGeP2_plot_main_CdGeP2_Boyd_118_o.py` (``../gallery/CdGeP2/plot_main_CdGeP2_Boyd_118_o.py``)
     - 00:00.565
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LaF3_plot_main_LaF3_Amotchkina.py` (``../gallery/LaF3/plot_main_LaF3_Amotchkina.py``)
     - 00:00.565
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ge_plot_main_Ge_Jellison.py` (``../gallery/Ge/plot_main_Ge_Jellison.py``)
     - 00:00.565
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZnSe_plot_main_ZnSe_Adachi.py` (``../gallery/ZnSe/plot_main_ZnSe_Adachi.py``)
     - 00:00.565
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Jung.py` (``../gallery/MoS2/plot_main_MoS2_Jung.py``)
     - 00:00.564
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaSO4_plot_main_CaSO4_Querry_alpha.py` (``../gallery/CaSO4/plot_main_CaSO4_Querry_alpha.py``)
     - 00:00.564
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cu_plot_main_Cu_Werner_DFT.py` (``../gallery/Cu/plot_main_Cu_Werner_DFT.py``)
     - 00:00.564
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaGdAlO4_plot_main_CaGdAlO4_Loiko_e.py` (``../gallery/CaGdAlO4/plot_main_CaGdAlO4_Loiko_e.py``)
     - 00:00.564
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa4Se7_plot_main_BaGa4Se7_Kato_beta.py` (``../gallery/BaGa4Se7/plot_main_BaGa4Se7_Kato_beta.py``)
     - 00:00.564
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Kofman_30K.py` (``../gallery/H2O/plot_main_H2O_Kofman_30K.py``)
     - 00:00.563
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdS_plot_main_CdS_Bieniewski_o.py` (``../gallery/CdS/plot_main_CdS_Bieniewski_o.py``)
     - 00:00.563
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaSO4_plot_main_CaSO4_Querry_gamma.py` (``../gallery/CaSO4/plot_main_CaSO4_Querry_gamma.py``)
     - 00:00.563
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Bi12GeO20_plot_main_Bi12GeO20_Simon.py` (``../gallery/Bi12GeO20/plot_main_Bi12GeO20_Simon.py``)
     - 00:00.563
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaP_plot_main_GaP_Adachi.py` (``../gallery/GaP/plot_main_GaP_Adachi.py``)
     - 00:00.563
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BeAl6O10_plot_main_BeAl6O10_Pestryakov_beta.py` (``../gallery/BeAl6O10/plot_main_BeAl6O10_Pestryakov_beta.py``)
     - 00:00.563
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaF2_plot_main_CaF2_Malitson.py` (``../gallery/CaF2/plot_main_CaF2_Malitson.py``)
     - 00:00.563
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgO_plot_main_MgO_Stephens.py` (``../gallery/MgO/plot_main_MgO_Stephens.py``)
     - 00:00.562
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_PbTiO3_plot_main_PbTiO3_Singh_e.py` (``../gallery/PbTiO3/plot_main_PbTiO3_Singh_e.py``)
     - 00:00.562
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cs_plot_main_Cs_Smith.py` (``../gallery/Cs/plot_main_Cs_Smith.py``)
     - 00:00.561
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Er_plot_main_Er_Larruquert.py` (``../gallery/Er/plot_main_Er_Larruquert.py``)
     - 00:00.560
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BN_plot_main_BN_Grudinin_DFT_o.py` (``../gallery/BN/plot_main_BN_Grudinin_DFT_o.py``)
     - 00:00.560
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaMoO4_plot_main_CaMoO4_Bond_e.py` (``../gallery/CaMoO4/plot_main_CaMoO4_Bond_e.py``)
     - 00:00.560
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CuGaS2_plot_main_CuGaS2_Boyd_20_o.py` (``../gallery/CuGaS2/plot_main_CuGaS2_Boyd_20_o.py``)
     - 00:00.559
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoSe2_plot_main_MoSe2_Hsu_2L.py` (``../gallery/MoSe2/plot_main_MoSe2_Hsu_2L.py``)
     - 00:00.559
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa4S7_plot_main_BaGa4S7_Badikov_alpha.py` (``../gallery/BaGa4S7/plot_main_BaGa4S7_Badikov_alpha.py``)
     - 00:00.559
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Song_sHOPG_e.py` (``../gallery/C/plot_main_C_Song_sHOPG_e.py``)
     - 00:00.558
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaB2O4_plot_main_BaB2O4_Eimerl_e.py` (``../gallery/BaB2O4/plot_main_BaB2O4_Eimerl_e.py``)
     - 00:00.558
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaAs_plot_main_GaAs_Jellison.py` (``../gallery/GaAs/plot_main_GaAs_Jellison.py``)
     - 00:00.558
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Lemarchand_3.96nm.py` (``../gallery/Au/plot_main_Au_Lemarchand_3.96nm.py``)
     - 00:00.558
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Djurisic_o.py` (``../gallery/C/plot_main_C_Djurisic_o.py``)
     - 00:00.557
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cu_plot_main_Cu_Ordal.py` (``../gallery/Cu/plot_main_Cu_Ordal.py``)
     - 00:00.557
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Be_plot_main_Be_Rakic_LD.py` (``../gallery/Be/plot_main_Be_Rakic_LD.py``)
     - 00:00.557
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KCl_plot_main_KCl_Li.py` (``../gallery/KCl/plot_main_KCl_Li.py``)
     - 00:00.556
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ni_plot_main_Ni_Rakic_BB.py` (``../gallery/Ni/plot_main_Ni_Rakic_BB.py``)
     - 00:00.556
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdSe_plot_main_CdSe_Lisitsa_e.py` (``../gallery/CdSe/plot_main_CdSe_Lisitsa_e.py``)
     - 00:00.556
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_B4C_plot_main_B4C_Larruquert.py` (``../gallery/B4C/plot_main_B4C_Larruquert.py``)
     - 00:00.556
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Larruquert.py` (``../gallery/C/plot_main_C_Larruquert.py``)
     - 00:00.556
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaF2_plot_main_CaF2_Daimon_20.py` (``../gallery/CaF2/plot_main_CaF2_Daimon_20.py``)
     - 00:00.556
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CuGaS2_plot_main_CuGaS2_Boyd_120_e.py` (``../gallery/CuGaS2/plot_main_CuGaS2_Boyd_120_e.py``)
     - 00:00.555
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaB2O4_plot_main_BaB2O4_Tamosauskas_o.py` (``../gallery/BaB2O4/plot_main_BaB2O4_Tamosauskas_o.py``)
     - 00:00.555
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Dore.py` (``../gallery/C/plot_main_C_Dore.py``)
     - 00:00.555
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Yakubovsky_117nm.py` (``../gallery/Au/plot_main_Au_Yakubovsky_117nm.py``)
     - 00:00.555
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa4Se7_plot_main_BaGa4Se7_Kato_gamma.py` (``../gallery/BaGa4Se7/plot_main_BaGa4Se7_Kato_gamma.py``)
     - 00:00.555
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaF2_plot_main_CaF2_Daimon_25.py` (``../gallery/CaF2/plot_main_CaF2_Daimon_25.py``)
     - 00:00.554
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BeAl2O4_plot_main_BeAl2O4_Walling_gamma.py` (``../gallery/BeAl2O4/plot_main_BeAl2O4_Walling_gamma.py``)
     - 00:00.554
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Bi12SiO20_plot_main_Bi12SiO20_Gospodinov.py` (``../gallery/Bi12SiO20/plot_main_Bi12SiO20_Gospodinov.py``)
     - 00:00.554
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaYAlO4_plot_main_CaYAlO4_Loiko_o.py` (``../gallery/CaYAlO4/plot_main_CaYAlO4_Loiko_o.py``)
     - 00:00.554
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Bi2Se3_plot_main_Bi2Se3_Fang_8L.py` (``../gallery/Bi2Se3/plot_main_Bi2Se3_Fang_8L.py``)
     - 00:00.554
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Bi4Ti3O12_plot_main_Bi4Ti3O12_Simon_b.py` (``../gallery/Bi4Ti3O12/plot_main_Bi4Ti3O12_Simon_b.py``)
     - 00:00.553
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Querry_AsburyMicro260.py` (``../gallery/C/plot_main_C_Querry_AsburyMicro260.py``)
     - 00:00.553
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaSb_plot_main_GaSb_Aspnes.py` (``../gallery/GaSb/plot_main_GaSb_Aspnes.py``)
     - 00:00.553
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdTe_plot_main_CdTe_Treharne.py` (``../gallery/CdTe/plot_main_CdTe_Treharne.py``)
     - 00:00.553
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CsI_plot_main_CsI_Rodney.py` (``../gallery/CsI/plot_main_CsI_Rodney.py``)
     - 00:00.553
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Hagemann.py` (``../gallery/C/plot_main_C_Hagemann.py``)
     - 00:00.552
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa4S7_plot_main_BaGa4S7_Badikov_gamma.py` (``../gallery/BaGa4S7/plot_main_BaGa4S7_Badikov_gamma.py``)
     - 00:00.551
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_WSe2_plot_main_WSe2_Hsu_3L.py` (``../gallery/WSe2/plot_main_WSe2_Hsu_3L.py``)
     - 00:00.551
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Lemarchand_5.77nm.py` (``../gallery/Au/plot_main_Au_Lemarchand_5.77nm.py``)
     - 00:00.551
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Be_plot_main_Be_Rakic_BB.py` (``../gallery/Be/plot_main_Be_Rakic_BB.py``)
     - 00:00.551
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Magnozzi_350C.py` (``../gallery/Au/plot_main_Au_Magnozzi_350C.py``)
     - 00:00.550
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Rosenblatt_11nm.py` (``../gallery/Au/plot_main_Au_Rosenblatt_11nm.py``)
     - 00:00.550
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CuGaS2_plot_main_CuGaS2_Boyd_20_e.py` (``../gallery/CuGaS2/plot_main_CuGaS2_Boyd_20_e.py``)
     - 00:00.550
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaF2_plot_main_BaF2_Querry.py` (``../gallery/BaF2/plot_main_BaF2_Querry.py``)
     - 00:00.550
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Ermolaev_SWCNT_o.py` (``../gallery/C/plot_main_C_Ermolaev_SWCNT_o.py``)
     - 00:00.550
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdTe_plot_main_CdTe_DeBell_20K.py` (``../gallery/CdTe/plot_main_CdTe_DeBell_20K.py``)
     - 00:00.549
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaS_plot_main_GaS_Kato_o.py` (``../gallery/GaS/plot_main_GaS_Kato_o.py``)
     - 00:00.549
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BN_plot_main_BN_Grudinin_e.py` (``../gallery/BN/plot_main_BN_Grudinin_e.py``)
     - 00:00.548
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Co_plot_main_Co_Werner.py` (``../gallery/Co/plot_main_Co_Werner.py``)
     - 00:00.548
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BP_plot_main_BP_Wettling.py` (``../gallery/BP/plot_main_BP_Wettling.py``)
     - 00:00.548
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaB2O4_plot_main_BaB2O4_Eimerl_o.py` (``../gallery/BaB2O4/plot_main_BaB2O4_Eimerl_o.py``)
     - 00:00.547
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa2GeSe6_plot_main_BaGa2GeSe6_Kato_e.py` (``../gallery/BaGa2GeSe6/plot_main_BaGa2GeSe6_Kato_e.py``)
     - 00:00.547
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaF2_plot_main_BaF2_Zheng_21C.py` (``../gallery/BaF2/plot_main_BaF2_Zheng_21C.py``)
     - 00:00.547
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CS2_plot_main_CS2_Ghosal.py` (``../gallery/CS2/plot_main_CS2_Ghosal.py``)
     - 00:00.547
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_As2S3_plot_main_As2S3_Slavich_alpha.py` (``../gallery/As2S3/plot_main_As2S3_Slavich_alpha.py``)
     - 00:00.545
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Tikuisis.py` (``../gallery/C/plot_main_C_Tikuisis.py``)
     - 00:00.545
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_In_plot_main_In_Golovashkin_295K.py` (``../gallery/In/plot_main_In_Golovashkin_295K.py``)
     - 00:00.544
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BeAl6O10_plot_main_BeAl6O10_Pestryakov_alpha.py` (``../gallery/BeAl6O10/plot_main_BeAl6O10_Pestryakov_alpha.py``)
     - 00:00.544
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Yakubovsky_53nm.py` (``../gallery/Au/plot_main_Au_Yakubovsky_53nm.py``)
     - 00:00.544
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_As2S3_plot_main_As2S3_Slavich_abinitio_gamma.py` (``../gallery/As2S3/plot_main_As2S3_Slavich_abinitio_gamma.py``)
     - 00:00.543
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Sinnock_solid_105K.py` (``../gallery/Kr/plot_main_Kr_Sinnock_solid_105K.py``)
     - 00:00.543
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa2GeSe6_plot_main_BaGa2GeSe6_Kato_o.py` (``../gallery/BaGa2GeSe6/plot_main_BaGa2GeSe6_Kato_o.py``)
     - 00:00.542
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Querry_DixonKS2.py` (``../gallery/C/plot_main_C_Querry_DixonKS2.py``)
     - 00:00.542
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Fe2O3_plot_main_Fe2O3_Querry_o.py` (``../gallery/Fe2O3/plot_main_Fe2O3_Querry_o.py``)
     - 00:00.542
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Borzsonyi.py` (``../gallery/Ar/plot_main_Ar_Borzsonyi.py``)
     - 00:00.541
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CaSO4_plot_main_CaSO4_Querry_beta.py` (``../gallery/CaSO4/plot_main_CaSO4_Querry_beta.py``)
     - 00:00.541
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Werner.py` (``../gallery/Au/plot_main_Au_Werner.py``)
     - 00:00.541
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Rosenblatt_44nm.py` (``../gallery/Au/plot_main_Au_Rosenblatt_44nm.py``)
     - 00:00.541
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdGeP2_plot_main_CdGeP2_Boyd_20_e.py` (``../gallery/CdGeP2/plot_main_CdGeP2_Boyd_20_e.py``)
     - 00:00.541
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Querry_Dixon1102.py` (``../gallery/C/plot_main_C_Querry_Dixon1102.py``)
     - 00:00.541
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CsF_plot_main_CsF_Li.py` (``../gallery/CsF/plot_main_CsF_Li.py``)
     - 00:00.540
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BN_plot_main_BN_Grudinin_DFT_e.py` (``../gallery/BN/plot_main_BN_Grudinin_DFT_e.py``)
     - 00:00.539
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_As2S3_plot_main_As2S3_Slavich_gamma.py` (``../gallery/As2S3/plot_main_As2S3_Slavich_gamma.py``)
     - 00:00.539
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Johnson.py` (``../gallery/Au/plot_main_Au_Johnson.py``)
     - 00:00.539
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Yakubovsky_25nm.py` (``../gallery/Au/plot_main_Au_Yakubovsky_25nm.py``)
     - 00:00.539
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BiB3O6_plot_main_BiB3O6_Umemura_beta.py` (``../gallery/BiB3O6/plot_main_BiB3O6_Umemura_beta.py``)
     - 00:00.539
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Werner_DFT.py` (``../gallery/Au/plot_main_Au_Werner_DFT.py``)
     - 00:00.539
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Song_Arc.py` (``../gallery/C/plot_main_C_Song_Arc.py``)
     - 00:00.539
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Magnozzi_225C.py` (``../gallery/Au/plot_main_Au_Magnozzi_225C.py``)
     - 00:00.539
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BeAl6O10_plot_main_BeAl6O10_Pestryakov_gamma.py` (``../gallery/BeAl6O10/plot_main_BeAl6O10_Pestryakov_gamma.py``)
     - 00:00.538
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Olmon_sc.py` (``../gallery/Au/plot_main_Au_Olmon_sc.py``)
     - 00:00.537
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Pr_plot_main_Pr_Fernandez_Perea.py` (``../gallery/Pr/plot_main_Pr_Fernandez_Perea.py``)
     - 00:00.537
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa4Se7_plot_main_BaGa4Se7_Kato_alpha.py` (``../gallery/BaGa4Se7/plot_main_BaGa4Se7_Kato_alpha.py``)
     - 00:00.537
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Schinke.py` (``../gallery/Si/plot_main_Si_Schinke.py``)
     - 00:00.535
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MoS2_plot_main_MoS2_Song_10L.py` (``../gallery/MoS2/plot_main_MoS2_Song_10L.py``)
     - 00:00.535
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_McPeak.py` (``../gallery/Au/plot_main_Au_McPeak.py``)
     - 00:00.535
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdGeP2_plot_main_CdGeP2_Boyd_20_o.py` (``../gallery/CdGeP2/plot_main_CdGeP2_Boyd_20_o.py``)
     - 00:00.534
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BN_plot_main_BN_Zotev_o.py` (``../gallery/BN/plot_main_BN_Zotev_o.py``)
     - 00:00.534
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CS2_plot_main_CS2_Chang.py` (``../gallery/CS2/plot_main_CS2_Chang.py``)
     - 00:00.534
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_GaS_plot_main_GaS_Zotev_o.py` (``../gallery/GaS/plot_main_GaS_Zotev_o.py``)
     - 00:00.534
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Song.py` (``../gallery/C/plot_main_C_Song.py``)
     - 00:00.533
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Bi4Ti3O12_plot_main_Bi4Ti3O12_Simon_a.py` (``../gallery/Bi4Ti3O12/plot_main_Bi4Ti3O12_Simon_a.py``)
     - 00:00.532
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_SrMoO4_plot_main_SrMoO4_Bond_o.py` (``../gallery/SrMoO4/plot_main_SrMoO4_Bond_o.py``)
     - 00:00.532
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaB2O4_plot_main_BaB2O4_Zhang_e.py` (``../gallery/BaB2O4/plot_main_BaB2O4_Zhang_e.py``)
     - 00:00.532
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Sinnock_solid_60K.py` (``../gallery/Ar/plot_main_Ar_Sinnock_solid_60K.py``)
     - 00:00.532
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cu_plot_main_Cu_Brimhall.py` (``../gallery/Cu/plot_main_Cu_Brimhall.py``)
     - 00:00.531
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_As2S3_plot_main_As2S3_Slavich_beta.py` (``../gallery/As2S3/plot_main_As2S3_Slavich_beta.py``)
     - 00:00.531
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdS_plot_main_CdS_Ninomiya_o.py` (``../gallery/CdS/plot_main_CdS_Ninomiya_o.py``)
     - 00:00.531
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Bideau_Mehu.py` (``../gallery/Ar/plot_main_Ar_Bideau_Mehu.py``)
     - 00:00.530
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Hagemann_2.py` (``../gallery/Au/plot_main_Au_Hagemann_2.py``)
     - 00:00.530
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Phillip.py` (``../gallery/C/plot_main_C_Phillip.py``)
     - 00:00.530
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Olmon_ev.py` (``../gallery/Au/plot_main_Au_Olmon_ev.py``)
     - 00:00.530
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Querry_Pyrolytic.py` (``../gallery/C/plot_main_C_Querry_Pyrolytic.py``)
     - 00:00.530
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_As2S3_plot_main_As2S3_Slavich_abinitio_beta.py` (``../gallery/As2S3/plot_main_As2S3_Slavich_abinitio_beta.py``)
     - 00:00.529
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Ciesielski_Ge.py` (``../gallery/Au/plot_main_Au_Ciesielski_Ge.py``)
     - 00:00.529
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaB2O4_plot_main_BaB2O4_Tamosauskas_e.py` (``../gallery/BaB2O4/plot_main_BaB2O4_Tamosauskas_e.py``)
     - 00:00.528
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BN_plot_main_BN_Zotev_e.py` (``../gallery/BN/plot_main_BN_Zotev_e.py``)
     - 00:00.528
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgF2_plot_main_MgF2_Zheng_e_21C.py` (``../gallery/MgF2/plot_main_MgF2_Zheng_e_21C.py``)
     - 00:00.528
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BeO_plot_main_BeO_Edwards_e.py` (``../gallery/BeO/plot_main_BeO_Edwards_e.py``)
     - 00:00.526
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BeO_plot_main_BeO_Edwards_o.py` (``../gallery/BeO/plot_main_BeO_Edwards_o.py``)
     - 00:00.526
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Sinnock_solid_40K.py` (``../gallery/Ar/plot_main_Ar_Sinnock_solid_40K.py``)
     - 00:00.526
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CuCl_plot_main_CuCl_Feldman.py` (``../gallery/CuCl/plot_main_CuCl_Feldman.py``)
     - 00:00.525
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaF2_plot_main_BaF2_Malitson.py` (``../gallery/BaF2/plot_main_BaF2_Malitson.py``)
     - 00:00.525
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AlSb_plot_main_AlSb_Adachi.py` (``../gallery/AlSb/plot_main_AlSb_Adachi.py``)
     - 00:00.524
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Sinnock_liquid_88K.py` (``../gallery/Ar/plot_main_Ar_Sinnock_liquid_88K.py``)
     - 00:00.524
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LaF3_plot_main_LaF3_Laihoa_o.py` (``../gallery/LaF3/plot_main_LaF3_Laihoa_o.py``)
     - 00:00.523
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Sinnock_liquid_83.81K.py` (``../gallery/Ar/plot_main_Ar_Sinnock_liquid_83.81K.py``)
     - 00:00.523
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa4Se7_plot_main_BaGa4Se7_Badikov_gamma.py` (``../gallery/BaGa4Se7/plot_main_BaGa4Se7_Badikov_gamma.py``)
     - 00:00.522
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Grace_liquid_83.81K.py` (``../gallery/Ar/plot_main_Ar_Grace_liquid_83.81K.py``)
     - 00:00.522
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Yakubovsky_9nm.py` (``../gallery/Au/plot_main_Au_Yakubovsky_9nm.py``)
     - 00:00.522
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Sinnock_solid_20K.py` (``../gallery/Ar/plot_main_Ar_Sinnock_solid_20K.py``)
     - 00:00.521
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiTaO3_plot_main_LiTaO3_Bond_e.py` (``../gallery/LiTaO3/plot_main_LiTaO3_Bond_e.py``)
     - 00:00.521
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AlN_plot_main_AlN_Beliaev1.py` (``../gallery/AlN/plot_main_AlN_Beliaev1.py``)
     - 00:00.521
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Sinnock_liquid_86K.py` (``../gallery/Ar/plot_main_Ar_Sinnock_liquid_86K.py``)
     - 00:00.521
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_As2Se3_plot_main_As2Se3_Joseph_400nm.py` (``../gallery/As2Se3/plot_main_As2Se3_Joseph_400nm.py``)
     - 00:00.520
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Windt.py` (``../gallery/Au/plot_main_Au_Windt.py``)
     - 00:00.520
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Peck_0C.py` (``../gallery/Ar/plot_main_Ar_Peck_0C.py``)
     - 00:00.520
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Sinnock_solid_83.81K.py` (``../gallery/Ar/plot_main_Ar_Sinnock_solid_83.81K.py``)
     - 00:00.519
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Grace_liquid_90K.py` (``../gallery/Ar/plot_main_Ar_Grace_liquid_90K.py``)
     - 00:00.519
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdS_plot_main_CdS_Bieniewski_e.py` (``../gallery/CdS/plot_main_CdS_Bieniewski_e.py``)
     - 00:00.518
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Li_plot_main_Li_Mathewson_298K.py` (``../gallery/Li/plot_main_Li_Mathewson_298K.py``)
     - 00:00.518
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Li_plot_main_Li_Mathewson_140K.py` (``../gallery/Li/plot_main_Li_Mathewson_140K.py``)
     - 00:00.518
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Sinnock_solid_50K.py` (``../gallery/Ar/plot_main_Ar_Sinnock_solid_50K.py``)
     - 00:00.518
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Rakic_BB.py` (``../gallery/Au/plot_main_Au_Rakic_BB.py``)
     - 00:00.517
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_As2Se3_plot_main_As2Se3_Joseph_700nm.py` (``../gallery/As2Se3/plot_main_As2Se3_Joseph_700nm.py``)
     - 00:00.516
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Mg_plot_main_Mg_Vidal_Dasilva.py` (``../gallery/Mg/plot_main_Mg_Vidal_Dasilva.py``)
     - 00:00.514
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Song_cHOPG_o.py` (``../gallery/C/plot_main_C_Song_cHOPG_o.py``)
     - 00:00.514
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Sinnock_liquid_118K.py` (``../gallery/Kr/plot_main_Kr_Sinnock_liquid_118K.py``)
     - 00:00.514
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Olmon_ts.py` (``../gallery/Au/plot_main_Au_Olmon_ts.py``)
     - 00:00.513
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Sinnock_solid_30K.py` (``../gallery/Ar/plot_main_Ar_Sinnock_solid_30K.py``)
     - 00:00.513
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Yakubovsky_6nm.py` (``../gallery/Au/plot_main_Au_Yakubovsky_6nm.py``)
     - 00:00.513
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Cu_plot_main_Cu_Rakic_BB.py` (``../gallery/Cu/plot_main_Cu_Rakic_BB.py``)
     - 00:00.513
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Magnozzi_25C.py` (``../gallery/Au/plot_main_Au_Magnozzi_25C.py``)
     - 00:00.512
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_As2S3_plot_main_As2S3_Rodney.py` (``../gallery/As2S3/plot_main_As2S3_Rodney.py``)
     - 00:00.512
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AlSb_plot_main_AlSb_Zollner.py` (``../gallery/AlSb/plot_main_AlSb_Zollner.py``)
     - 00:00.512
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Ciesielski.py` (``../gallery/Au/plot_main_Au_Ciesielski.py``)
     - 00:00.511
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Li_plot_main_Li_Rasigni.py` (``../gallery/Li/plot_main_Li_Rasigni.py``)
     - 00:00.511
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Lu_plot_main_Lu_Garcia_Cortes.py` (``../gallery/Lu/plot_main_Lu_Garcia_Cortes.py``)
     - 00:00.511
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Sik_700C.py` (``../gallery/Si/plot_main_Si_Sik_700C.py``)
     - 00:00.509
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Querry_Dixon200_10.py` (``../gallery/C/plot_main_C_Querry_Dixon200_10.py``)
     - 00:00.509
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Bi_plot_main_Bi_Werner_DFT.py` (``../gallery/Bi/plot_main_Bi_Werner_DFT.py``)
     - 00:00.509
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Bi_plot_main_Bi_Hagemann.py` (``../gallery/Bi/plot_main_Bi_Hagemann.py``)
     - 00:00.508
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_100K.py` (``../gallery/Si/plot_main_Si_Li_100K.py``)
     - 00:00.505
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Sinnock_solid_70K.py` (``../gallery/Ar/plot_main_Ar_Sinnock_solid_70K.py``)
     - 00:00.505
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Yakubovsky_4nm.py` (``../gallery/Au/plot_main_Au_Yakubovsky_4nm.py``)
     - 00:00.504
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Babar.py` (``../gallery/Au/plot_main_Au_Babar.py``)
     - 00:00.504
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa4S7_plot_main_BaGa4S7_Badikov_beta.py` (``../gallery/BaGa4S7/plot_main_BaGa4S7_Badikov_beta.py``)
     - 00:00.504
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YVO4_plot_main_YVO4_Shi_e_20C.py` (``../gallery/YVO4/plot_main_YVO4_Shi_e_20C.py``)
     - 00:00.503
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AlN_plot_main_AlN_Beliaev2.py` (``../gallery/AlN/plot_main_AlN_Beliaev2.py``)
     - 00:00.503
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Rakic_LD.py` (``../gallery/Au/plot_main_Au_Rakic_LD.py``)
     - 00:00.502
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al_plot_main_Al_McPeak.py` (``../gallery/Al/plot_main_Al_McPeak.py``)
     - 00:00.502
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al2O3_plot_main_Al2O3_Zhukovsky.py` (``../gallery/Al2O3/plot_main_Al2O3_Zhukovsky.py``)
     - 00:00.502
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al2O3_plot_main_Al2O3_Malitson_o.py` (``../gallery/Al2O3/plot_main_Al2O3_Malitson_o.py``)
     - 00:00.501
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al_plot_main_Al_Larruquert.py` (``../gallery/Al/plot_main_Al_Larruquert.py``)
     - 00:00.500
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaTiO3_plot_main_BaTiO3_Wemple_e.py` (``../gallery/BaTiO3/plot_main_BaTiO3_Wemple_e.py``)
     - 00:00.499
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiB3O5_plot_main_LiB3O5_Chen_beta.py` (``../gallery/LiB3O5/plot_main_LiB3O5_Chen_beta.py``)
     - 00:00.499
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al2O3_plot_main_Al2O3_Querry_e.py` (``../gallery/Al2O3/plot_main_Al2O3_Querry_e.py``)
     - 00:00.499
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Hagemann.py` (``../gallery/Au/plot_main_Au_Hagemann.py``)
     - 00:00.499
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_CdTe_plot_main_CdTe_Adachi.py` (``../gallery/CdTe/plot_main_CdTe_Adachi.py``)
     - 00:00.497
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AlAs_plot_main_AlAs_Fern.py` (``../gallery/AlAs/plot_main_AlAs_Fern.py``)
     - 00:00.497
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AlSb_plot_main_AlSb_Djurisic.py` (``../gallery/AlSb/plot_main_AlSb_Djurisic.py``)
     - 00:00.497
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa4Se7_plot_main_BaGa4Se7_Badikov_beta.py` (``../gallery/BaGa4Se7/plot_main_BaGa4Se7_Badikov_beta.py``)
     - 00:00.496
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al2O3_plot_main_Al2O3_Malitson_e.py` (``../gallery/Al2O3/plot_main_Al2O3_Malitson_e.py``)
     - 00:00.496
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Sinnock_solid_80K.py` (``../gallery/Ar/plot_main_Ar_Sinnock_solid_80K.py``)
     - 00:00.495
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AlPO4_plot_main_AlPO4_Bond_e.py` (``../gallery/AlPO4/plot_main_AlPO4_Bond_e.py``)
     - 00:00.495
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Sinnock_liquid_126K.py` (``../gallery/Kr/plot_main_Kr_Sinnock_liquid_126K.py``)
     - 00:00.494
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Larsen.py` (``../gallery/Ar/plot_main_Ar_Larsen.py``)
     - 00:00.494
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Sinnock_liquid_90K.py` (``../gallery/Ar/plot_main_Ar_Sinnock_liquid_90K.py``)
     - 00:00.493
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa4S7_plot_main_BaGa4S7_Kato_gamma.py` (``../gallery/BaGa4S7/plot_main_BaGa4S7_Kato_gamma.py``)
     - 00:00.492
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AlN_plot_main_AlN_Pastrnak_o.py` (``../gallery/AlN/plot_main_AlN_Pastrnak_o.py``)
     - 00:00.492
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Zr_plot_main_Zr_Palm.py` (``../gallery/Zr/plot_main_Zr_Palm.py``)
     - 00:00.492
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al_plot_main_Al_Hagemann.py` (``../gallery/Al/plot_main_Al_Hagemann.py``)
     - 00:00.491
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa4S7_plot_main_BaGa4S7_Kato_beta.py` (``../gallery/BaGa4S7/plot_main_BaGa4S7_Kato_beta.py``)
     - 00:00.490
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgGaSe2_plot_main_AgGaSe2_Kato_e.py` (``../gallery/AgGaSe2/plot_main_AgGaSe2_Kato_e.py``)
     - 00:00.488
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Fe_plot_main_Fe_Ordal.py` (``../gallery/Fe/plot_main_Fe_Ordal.py``)
     - 00:00.488
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al_plot_main_Al_Mathewson.py` (``../gallery/Al/plot_main_Al_Mathewson.py``)
     - 00:00.487
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al2O3_plot_main_Al2O3_Malitson.py` (``../gallery/Al2O3/plot_main_Al2O3_Malitson.py``)
     - 00:00.487
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al2O3_plot_main_Al2O3_Kischkat.py` (``../gallery/Al2O3/plot_main_Al2O3_Kischkat.py``)
     - 00:00.487
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_H2O_plot_main_H2O_Warren_2008.py` (``../gallery/H2O/plot_main_H2O_Warren_2008.py``)
     - 00:00.487
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al_plot_main_Al_Rakic.py` (``../gallery/Al/plot_main_Al_Rakic.py``)
     - 00:00.486
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al2O3_plot_main_Al2O3_Boidin.py` (``../gallery/Al2O3/plot_main_Al2O3_Boidin.py``)
     - 00:00.486
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al2O3_plot_main_Al2O3_Hagemann.py` (``../gallery/Al2O3/plot_main_Al2O3_Hagemann.py``)
     - 00:00.485
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AlN_plot_main_AlN_Pastrnak_e.py` (``../gallery/AlN/plot_main_AlN_Pastrnak_e.py``)
     - 00:00.485
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Grace_solid_83.81K.py` (``../gallery/Ar/plot_main_Ar_Grace_solid_83.81K.py``)
     - 00:00.484
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al2O3_plot_main_Al2O3_Querry.py` (``../gallery/Al2O3/plot_main_Al2O3_Querry.py``)
     - 00:00.484
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al_plot_main_Al_Rakic_BB.py` (``../gallery/Al/plot_main_Al_Rakic_BB.py``)
     - 00:00.483
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al_plot_main_Al_Cheng.py` (``../gallery/Al/plot_main_Al_Cheng.py``)
     - 00:00.481
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgGaSe2_plot_main_AgGaSe2_Kato_o.py` (``../gallery/AgGaSe2/plot_main_AgGaSe2_Kato_o.py``)
     - 00:00.480
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_liquid_166K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_liquid_166K.py``)
     - 00:00.480
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Lu2O3_plot_main_Lu2O3_Yao.py` (``../gallery/Lu2O3/plot_main_Lu2O3_Yao.py``)
     - 00:00.479
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AlN_plot_main_AlN_Kischkat.py` (``../gallery/AlN/plot_main_AlN_Kischkat.py``)
     - 00:00.475
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgCl_plot_main_AgCl_Tilton.py` (``../gallery/AgCl/plot_main_AgCl_Tilton.py``)
     - 00:00.474
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al_plot_main_Al_Rakic_LD.py` (``../gallery/Al/plot_main_Al_Rakic_LD.py``)
     - 00:00.474
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_BaGa4Se7_plot_main_BaGa4Se7_Badikov_alpha.py` (``../gallery/BaGa4Se7/plot_main_BaGa4Se7_Badikov_alpha.py``)
     - 00:00.474
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Mg_plot_main_Mg_Palm.py` (``../gallery/Mg/plot_main_Mg_Palm.py``)
     - 00:00.473
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgGaSe2_plot_main_AgGaSe2_Boyd_o.py` (``../gallery/AgGaSe2/plot_main_AgGaSe2_Boyd_o.py``)
     - 00:00.473
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgGaSe2_plot_main_AgGaSe2_Harasaki_e.py` (``../gallery/AgGaSe2/plot_main_AgGaSe2_Harasaki_e.py``)
     - 00:00.472
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al_plot_main_Al_Ordal.py` (``../gallery/Al/plot_main_Al_Ordal.py``)
     - 00:00.471
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MnPSe3_plot_main_MnPSe3_Zotev.py` (``../gallery/MnPSe3/plot_main_MnPSe3_Zotev.py``)
     - 00:00.471
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Al2O3_plot_main_Al2O3_Querry_o.py` (``../gallery/Al2O3/plot_main_Al2O3_Querry_o.py``)
     - 00:00.471
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Lu2O3_plot_main_Lu2O3_Kaminskii.py` (``../gallery/Lu2O3/plot_main_Lu2O3_Kaminskii.py``)
     - 00:00.471
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Na_plot_main_Na_Yamaguchi.py` (``../gallery/Na/plot_main_Na_Yamaguchi.py``)
     - 00:00.470
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Zr_plot_main_Zr_Windt.py` (``../gallery/Zr/plot_main_Zr_Windt.py``)
     - 00:00.468
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgGaS2_plot_main_AgGaS2_Takaoka_e.py` (``../gallery/AgGaS2/plot_main_AgGaS2_Takaoka_e.py``)
     - 00:00.468
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LuAl3_BO3_4_plot_main_LuAl3_BO3_4_Fang_o.py` (``../gallery/LuAl3_BO3_4/plot_main_LuAl3_BO3_4_Fang_o.py``)
     - 00:00.467
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgBr_plot_main_AgBr_Schroter.py` (``../gallery/AgBr/plot_main_AgBr_Schroter.py``)
     - 00:00.467
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZrTe5_plot_main_ZrTe5_Guo_beta.py` (``../gallery/ZrTe5/plot_main_ZrTe5_Guo_beta.py``)
     - 00:00.467
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Y3Al5O12_plot_main_Y3Al5O12_Hrabovsky.py` (``../gallery/Y3Al5O12/plot_main_Y3Al5O12_Hrabovsky.py``)
     - 00:00.467
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AlPO4_plot_main_AlPO4_Bond_o.py` (``../gallery/AlPO4/plot_main_AlPO4_Bond_o.py``)
     - 00:00.467
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZrTe5_plot_main_ZrTe5_Guo_gamma.py` (``../gallery/ZrTe5/plot_main_ZrTe5_Guo_gamma.py``)
     - 00:00.467
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag3AsS3_plot_main_Ag3AsS3_Hulme_o.py` (``../gallery/Ag3AsS3/plot_main_Ag3AsS3_Hulme_o.py``)
     - 00:00.467
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag3AsS3_plot_main_Ag3AsS3_Hulme_e.py` (``../gallery/Ag3AsS3/plot_main_Ag3AsS3_Hulme_e.py``)
     - 00:00.467
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_KTiOPO4_plot_main_KTiOPO4_Kato_alpha.py` (``../gallery/KTiOPO4/plot_main_KTiOPO4_Kato_alpha.py``)
     - 00:00.466
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YVO4_plot_main_YVO4_Shi_o_20C.py` (``../gallery/YVO4/plot_main_YVO4_Shi_o_20C.py``)
     - 00:00.465
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgGaSe2_plot_main_AgGaSe2_Harasaki_o.py` (``../gallery/AgGaSe2/plot_main_AgGaSe2_Harasaki_o.py``)
     - 00:00.464
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Si_plot_main_Si_Li_500K.py` (``../gallery/Si/plot_main_Si_Li_500K.py``)
     - 00:00.464
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiGaS2_plot_main_LiGaS2_Kato_gamma.py` (``../gallery/LiGaS2/plot_main_LiGaS2_Kato_gamma.py``)
     - 00:00.463
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiTaO3_plot_main_LiTaO3_Bond_o.py` (``../gallery/LiTaO3/plot_main_LiTaO3_Bond_o.py``)
     - 00:00.462
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ar_plot_main_Ar_Grace_solid_20K.py` (``../gallery/Ar/plot_main_Ar_Grace_solid_20K.py``)
     - 00:00.460
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgGaS2_plot_main_AgGaS2_Boyd_e.py` (``../gallery/AgGaS2/plot_main_AgGaS2_Boyd_e.py``)
     - 00:00.460
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgBr_plot_main_AgBr_Polyanskiy.py` (``../gallery/AgBr/plot_main_AgBr_Polyanskiy.py``)
     - 00:00.459
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YVO4_plot_main_YVO4_Shi_e_80C.py` (``../gallery/YVO4/plot_main_YVO4_Shi_e_80C.py``)
     - 00:00.459
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Ciesielski_Ni.py` (``../gallery/Ag/plot_main_Ag_Ciesielski_Ni.py``)
     - 00:00.459
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgGaSe2_plot_main_AgGaSe2_Boyd_e.py` (``../gallery/AgGaSe2/plot_main_AgGaSe2_Boyd_e.py``)
     - 00:00.457
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_O2_plot_main_O2_Zhang.py` (``../gallery/O2/plot_main_O2_Zhang.py``)
     - 00:00.457
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgGaS2_plot_main_AgGaS2_Kato_o.py` (``../gallery/AgGaS2/plot_main_AgGaS2_Kato_o.py``)
     - 00:00.457
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Y3Al5O12_plot_main_Y3Al5O12_Bond.py` (``../gallery/Y3Al5O12/plot_main_Y3Al5O12_Bond.py``)
     - 00:00.456
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_NiPS3_plot_main_NiPS3_Zotev.py` (``../gallery/NiPS3/plot_main_NiPS3_Zotev.py``)
     - 00:00.453
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LuAl3_BO3_4_plot_main_LuAl3_BO3_4_Fang_e.py` (``../gallery/LuAl3_BO3_4/plot_main_LuAl3_BO3_4_Fang_e.py``)
     - 00:00.453
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Lu3Al5O12_plot_main_Lu3Al5O12_Hrabovsky.py` (``../gallery/Lu3Al5O12/plot_main_Lu3Al5O12_Hrabovsky.py``)
     - 00:00.452
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AlAs_plot_main_AlAs_Rakic.py` (``../gallery/AlAs/plot_main_AlAs_Rakic.py``)
     - 00:00.452
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiNbO3_plot_main_LiNbO3_Zelmon_e.py` (``../gallery/LiNbO3/plot_main_LiNbO3_Zelmon_e.py``)
     - 00:00.452
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgGaS2_plot_main_AgGaS2_Takaoka_o.py` (``../gallery/AgGaS2/plot_main_AgGaS2_Takaoka_o.py``)
     - 00:00.451
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Sinnock_solid_115.95K.py` (``../gallery/Kr/plot_main_Kr_Sinnock_solid_115.95K.py``)
     - 00:00.451
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Ferrera_501K.py` (``../gallery/Ag/plot_main_Ag_Ferrera_501K.py``)
     - 00:00.451
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Y3Al5O12_plot_main_Y3Al5O12_Zelmon.py` (``../gallery/Y3Al5O12/plot_main_Y3Al5O12_Zelmon.py``)
     - 00:00.451
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_ZrTe5_plot_main_ZrTe5_Guo_alpha.py` (``../gallery/ZrTe5/plot_main_ZrTe5_Guo_alpha.py``)
     - 00:00.451
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Ferrera_298K.py` (``../gallery/Ag/plot_main_Ag_Ferrera_298K.py``)
     - 00:00.449
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LaF3_plot_main_LaF3_Rodriguez_de_Marcos.py` (``../gallery/LaF3/plot_main_LaF3_Rodriguez_de_Marcos.py``)
     - 00:00.449
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YLiF4_plot_main_YLiF4_Barnes_o.py` (``../gallery/YLiF4/plot_main_YLiF4_Barnes_o.py``)
     - 00:00.448
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_liquid_170K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_liquid_170K.py``)
     - 00:00.447
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Ferrera_404K.py` (``../gallery/Ag/plot_main_Ag_Ferrera_404K.py``)
     - 00:00.447
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Ferrera_600K.py` (``../gallery/Ag/plot_main_Ag_Ferrera_600K.py``)
     - 00:00.447
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Lemarchand_11.7nm.py` (``../gallery/Au/plot_main_Au_Lemarchand_11.7nm.py``)
     - 00:00.446
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgF2_plot_main_MgF2_Li_e.py` (``../gallery/MgF2/plot_main_MgF2_Li_e.py``)
     - 00:00.446
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Xe_plot_main_Xe_Sinnock_liquid_174K.py` (``../gallery/Xe/plot_main_Xe_Sinnock_liquid_174K.py``)
     - 00:00.446
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Mg_plot_main_Mg_Hagemann.py` (``../gallery/Mg/plot_main_Mg_Hagemann.py``)
     - 00:00.446
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiCaAlF6_plot_main_LiCaAlF6_Woods_e.py` (``../gallery/LiCaAlF6/plot_main_LiCaAlF6_Woods_e.py``)
     - 00:00.444
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Ciesielski.py` (``../gallery/Ag/plot_main_Ag_Ciesielski.py``)
     - 00:00.442
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_C_plot_main_C_Song_HiPco.py` (``../gallery/C/plot_main_C_Song_HiPco.py``)
     - 00:00.441
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgGaS2_plot_main_AgGaS2_Kato_e.py` (``../gallery/AgGaS2/plot_main_AgGaS2_Kato_e.py``)
     - 00:00.440
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_YLiF4_plot_main_YLiF4_Barnes_e.py` (``../gallery/YLiF4/plot_main_YLiF4_Barnes_e.py``)
     - 00:00.439
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LaF3_plot_main_LaF3_Laihoa_e.py` (``../gallery/LaF3/plot_main_LaF3_Laihoa_e.py``)
     - 00:00.437
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_AgGaS2_plot_main_AgGaS2_Boyd_o.py` (``../gallery/AgGaS2/plot_main_AgGaS2_Boyd_o.py``)
     - 00:00.436
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_MgAl2O4_plot_main_MgAl2O4_Tropf.py` (``../gallery/MgAl2O4/plot_main_MgAl2O4_Tropf.py``)
     - 00:00.435
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiB3O5_plot_main_LiB3O5_Chen_gamma.py` (``../gallery/LiB3O5/plot_main_LiB3O5_Chen_gamma.py``)
     - 00:00.435
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Lu2O3_plot_main_Lu2O3_Medenbach.py` (``../gallery/Lu2O3/plot_main_Lu2O3_Medenbach.py``)
     - 00:00.433
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Stahrenberg.py` (``../gallery/Ag/plot_main_Ag_Stahrenberg.py``)
     - 00:00.433
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiGaS2_plot_main_LiGaS2_Kato_alpha.py` (``../gallery/LiGaS2/plot_main_LiGaS2_Kato_alpha.py``)
     - 00:00.432
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Li_plot_main_Li_Callcott.py` (``../gallery/Li/plot_main_Li_Callcott.py``)
     - 00:00.432
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Johnson.py` (``../gallery/Ag/plot_main_Ag_Johnson.py``)
     - 00:00.431
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Li_plot_main_Li_Inagaki.py` (``../gallery/Li/plot_main_Li_Inagaki.py``)
     - 00:00.430
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiBr_plot_main_LiBr_Li.py` (``../gallery/LiBr/plot_main_LiBr_Li.py``)
     - 00:00.429
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Kr_plot_main_Kr_Sinnock_liquid_122K.py` (``../gallery/Kr/plot_main_Kr_Sinnock_liquid_122K.py``)
     - 00:00.426
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiIO3_plot_main_LiIO3_Herbst_o.py` (``../gallery/LiIO3/plot_main_LiIO3_Herbst_o.py``)
     - 00:00.426
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ni_plot_main_Ni_Werner_DFT.py` (``../gallery/Ni/plot_main_Ni_Werner_DFT.py``)
     - 00:00.425
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Ciesielski_Ge.py` (``../gallery/Ag/plot_main_Ag_Ciesielski_Ge.py``)
     - 00:00.424
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiB3O5_plot_main_LiB3O5_Chen_alpha.py` (``../gallery/LiB3O5/plot_main_LiB3O5_Chen_alpha.py``)
     - 00:00.423
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiCl_plot_main_LiCl_Li.py` (``../gallery/LiCl/plot_main_LiCl_Li.py``)
     - 00:00.422
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Au_plot_main_Au_Lemarchand_4.62nm.py` (``../gallery/Au/plot_main_Au_Lemarchand_4.62nm.py``)
     - 00:00.422
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiGaS2_plot_main_LiGaS2_Kato_beta.py` (``../gallery/LiGaS2/plot_main_LiGaS2_Kato_beta.py``)
     - 00:00.418
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Werner_DFT.py` (``../gallery/Ag/plot_main_Ag_Werner_DFT.py``)
     - 00:00.418
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Jiang.py` (``../gallery/Ag/plot_main_Ag_Jiang.py``)
     - 00:00.415
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiCaAlF6_plot_main_LiCaAlF6_Woods_o.py` (``../gallery/LiCaAlF6/plot_main_LiCaAlF6_Woods_o.py``)
     - 00:00.415
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Windt.py` (``../gallery/Ag/plot_main_Ag_Windt.py``)
     - 00:00.413
     - 230.5
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Rakic_BB.py` (``../gallery/Ag/plot_main_Ag_Rakic_BB.py``)
     - 00:00.413
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Hagemann.py` (``../gallery/Ag/plot_main_Ag_Hagemann.py``)
     - 00:00.412
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiI_plot_main_LiI_Li.py` (``../gallery/LiI/plot_main_LiI_Li.py``)
     - 00:00.412
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Rakic_LD.py` (``../gallery/Ag/plot_main_Ag_Rakic_LD.py``)
     - 00:00.412
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiIO3_plot_main_LiIO3_Umegaki_e.py` (``../gallery/LiIO3/plot_main_LiIO3_Umegaki_e.py``)
     - 00:00.411
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiIO3_plot_main_LiIO3_Umegaki_o.py` (``../gallery/LiIO3/plot_main_LiIO3_Umegaki_o.py``)
     - 00:00.410
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Babar.py` (``../gallery/Ag/plot_main_Ag_Babar.py``)
     - 00:00.406
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_McPeak.py` (``../gallery/Ag/plot_main_Ag_McPeak.py``)
     - 00:00.406
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiF_plot_main_LiF_Li.py` (``../gallery/LiF/plot_main_LiF_Li.py``)
     - 00:00.404
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_LiIO3_plot_main_LiIO3_Herbst_e.py` (``../gallery/LiIO3/plot_main_LiIO3_Herbst_e.py``)
     - 00:00.402
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Werner.py` (``../gallery/Ag/plot_main_Ag_Werner.py``)
     - 00:00.401
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Yang.py` (``../gallery/Ag/plot_main_Ag_Yang.py``)
     - 00:00.377
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Wu.py` (``../gallery/Ag/plot_main_Ag_Wu.py``)
     - 00:00.367
     - 230.4
   * - :ref:`sphx_glr_auto_gallery_Ag_plot_main_Ag_Choi.py` (``../gallery/Ag/plot_main_Ag_Choi.py``)
     - 00:00.367
     - 230.4
