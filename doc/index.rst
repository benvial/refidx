.. refidx documentation main file

=========
refidx
=========


.. include:: ../README.md
   :parser: myst_parser.sphinx_ 
   :start-after: <!-- start-badges -->
   :end-before: <!-- end-badges -->

Welcome! This is the documentation for ``refidx`` |release|, last updated on |today|.

``refidx`` is a package to get the refractive index of a material at a given wavelength
from the refractiveindex.info_ database.

.. _refractiveindex.info:
   https://refractiveindex.info/





.. toctree::
   :maxdepth: 2
   :caption: Quickstart
   :hidden:
   
   quickstart
   cite
   
   
.. toctree::
   :maxdepth: 2
   :caption: Examples
   :hidden:

   auto_examples/index



.. toctree::
   :caption: Gallery
   :hidden:

   auto_gallery/index

.. toctree::
  :maxdepth: 2
  :caption: API reference:
  :hidden:
  
  source/modules
  coverage
  idx

