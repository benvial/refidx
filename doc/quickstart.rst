.. _installation:

Installation
==============

.. note:: Python 3.8 or later is required.

.. _pip-install:

pip install
------------

The easiest way to install refidx is with pip:

.. code-block:: bash

  pip install refidx

.. _from-source:

From source
------------

To install refidx from source, first clone the repository:

.. code-block:: bash

  git clone https://gitlab.com/benvial/refidx.git

Then, install using pip:

.. code-block:: bash

  cd refidx
  pip install .

.. _develop:

Development
------------

If you want to contribute to refidx, you might want to install it in editable
mode:

.. code-block:: bash

  cd refidx
  pip install -e .

To install all the packages needed for development, testing and documentation, run:

.. code-block:: bash

  make dev
  make test-req
  make doc-req


To update the database:

.. code-block:: bash

  make dldb
  make gendb



To run all tests, run:

.. code-block:: bash

  make test


To build the gallery, run:

.. code-block:: bash

  make gengallery


To build the documentation, run:

.. code-block:: bash

  make doc

